package sklep;

import javax.swing.JOptionPane;

public class ZarzadzanieZamowieniami extends javax.swing.JFrame {
    
    public static int current_record = 0;
    
    public ZarzadzanieZamowieniami() {
        initComponents();
        changeValues();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        close_button = new javax.swing.JButton();
        nazwa_firmy_label = new javax.swing.JLabel();
        przedstawiciel_label = new javax.swing.JLabel();
        firma = new javax.swing.JTextField();
        przedstawiciel = new javax.swing.JTextField();
        telefon_label = new javax.swing.JLabel();
        telefon = new javax.swing.JTextField();
        email_label = new javax.swing.JLabel();
        email = new javax.swing.JTextField();
        data_zlozenia_label = new javax.swing.JLabel();
        data_zlozenia = new javax.swing.JTextField();
        data_przyjecia_label = new javax.swing.JLabel();
        data_przyjecia = new javax.swing.JTextField();
        platnosc_label = new javax.swing.JLabel();
        platnosc = new javax.swing.JTextField();
        data_wysylki_label = new javax.swing.JLabel();
        data_wysylki = new javax.swing.JTextField();
        stan_label = new javax.swing.JLabel();
        stan = new javax.swing.JTextField();
        prev_record = new javax.swing.JButton();
        next_record = new javax.swing.JButton();
        data_realizacji_label = new javax.swing.JLabel();
        data_realizacji = new javax.swing.JTextField();
        szczegoly = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("ZAMÓWIENIA");

        close_button.setBackground(new java.awt.Color(255, 0, 51));
        close_button.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        close_button.setForeground(new java.awt.Color(255, 255, 255));
        close_button.setText("Zamknij okno");
        close_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close_buttonActionPerformed(evt);
            }
        });

        nazwa_firmy_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        nazwa_firmy_label.setText("Nazwa firmy:");

        przedstawiciel_label.setFont(new java.awt.Font("Segoe UI Light", 0, 10)); // NOI18N
        przedstawiciel_label.setText("Imię i nazwisko przedstawiciela:");

        firma.setEditable(false);
        firma.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        przedstawiciel.setEditable(false);
        przedstawiciel.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        telefon_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        telefon_label.setText("Nr telefonu przedstawiciela:");

        telefon.setEditable(false);
        telefon.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        email_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        email_label.setText("E-mail przedstawiciela:");

        email.setEditable(false);
        email.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_zlozenia_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zlozenia_label.setText("Data złożenia zamówienia:");

        data_zlozenia.setEditable(false);
        data_zlozenia.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_przyjecia_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_przyjecia_label.setText("Data przyjęcia zamówienia:");

        data_przyjecia.setEditable(false);
        data_przyjecia.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        platnosc_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        platnosc_label.setText("Rodzaj płatności:");

        platnosc.setEditable(false);
        platnosc.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_wysylki_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_wysylki_label.setText("Data wysyłki:");

        data_wysylki.setEditable(false);
        data_wysylki.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        stan_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        stan_label.setText("Stan realizacji zamówienia:");

        stan.setEditable(false);
        stan.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        prev_record.setBackground(new java.awt.Color(27, 161, 226));
        prev_record.setFont(new java.awt.Font("Segoe UI Light", 1, 8)); // NOI18N
        prev_record.setForeground(new java.awt.Color(255, 255, 255));
        prev_record.setText("POPRZEDNIE");
        prev_record.setMargin(new java.awt.Insets(2, 0, 2, 0));
        prev_record.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prev_recordActionPerformed(evt);
            }
        });

        next_record.setBackground(new java.awt.Color(27, 161, 226));
        next_record.setFont(new java.awt.Font("Segoe UI Light", 1, 8)); // NOI18N
        next_record.setForeground(new java.awt.Color(255, 255, 255));
        next_record.setText("NASTĘPNE");
        next_record.setMargin(new java.awt.Insets(2, 0, 2, 0));
        next_record.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                next_recordActionPerformed(evt);
            }
        });

        data_realizacji_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_realizacji_label.setText("Data realizacji zamówienia:");

        data_realizacji.setEditable(false);
        data_realizacji.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        szczegoly.setBackground(new java.awt.Color(255, 51, 0));
        szczegoly.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        szczegoly.setForeground(new java.awt.Color(255, 255, 255));
        szczegoly.setText("Zobacz szczegóły zamówienia");
        szczegoly.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                szczegolyActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(prev_record, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(next_record, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(szczegoly, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(nazwa_firmy_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(44, 44, 44)
                        .addComponent(firma, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(26, 26, 26)
                        .addComponent(close_button))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(przedstawiciel_label, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(email_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(telefon_label, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                                    .addComponent(platnosc_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(data_wysylki_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(stan_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(data_zlozenia_label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(data_przyjecia_label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addComponent(data_realizacji_label, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(telefon, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                                .addComponent(email)
                                .addComponent(data_zlozenia)
                                .addComponent(data_przyjecia)
                                .addComponent(platnosc)
                                .addComponent(data_wysylki)
                                .addComponent(stan)
                                .addComponent(data_realizacji))
                            .addComponent(przedstawiciel))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(close_button)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nazwa_firmy_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(firma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(przedstawiciel_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(przedstawiciel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(telefon_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(telefon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(email_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(data_zlozenia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(data_zlozenia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(data_przyjecia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(data_przyjecia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(platnosc_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(platnosc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(data_wysylki_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(data_wysylki, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stan_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(data_realizacji_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(data_realizacji, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(next_record, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(prev_record, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addComponent(szczegoly, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void changeValues() {
        firma.setText(Sklep_komputerowy.zamowienia.get(current_record).getK().getNazwaFirmy());
        przedstawiciel.setText(Sklep_komputerowy.zamowienia.get(current_record).getK().getImie() + " " 
                + Sklep_komputerowy.zamowienia.get(current_record).getK().getNazwisko());
        telefon.setText(Sklep_komputerowy.zamowienia.get(current_record).getK().getTelefon());
        email.setText(Sklep_komputerowy.zamowienia.get(current_record).getK().getEmail());
        data_zlozenia.setText(Sklep_komputerowy.zamowienia.get(current_record).getData_zamowienia());
        data_przyjecia.setText(Sklep_komputerowy.zamowienia.get(current_record).getData_przyjecia_zamowienia());
        platnosc.setText(Sklep_komputerowy.zamowienia.get(current_record).getPlatnosc());
        data_wysylki.setText(Sklep_komputerowy.zamowienia.get(current_record).getData_wysylki());
        stan.setText(Boolean.toString(Sklep_komputerowy.zamowienia.get(current_record).getCzy_zrealizowano()));
        data_realizacji.setText(Sklep_komputerowy.zamowienia.get(current_record).getData_realizacji());
    }
    
    
    private void close_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_close_buttonActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_close_buttonActionPerformed

    private void prev_recordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prev_recordActionPerformed
        if (current_record>0){
            current_record--;
            changeValues();
        }
    }//GEN-LAST:event_prev_recordActionPerformed

    private void next_recordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_next_recordActionPerformed
        if (current_record<Sklep_komputerowy.zamowienia.size()-1) current_record++;
        changeValues();
    }//GEN-LAST:event_next_recordActionPerformed

    private void szczegolyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_szczegolyActionPerformed
        //try {
            SzczegolyZamowienia sz = new SzczegolyZamowienia();
            sz.setVisible(true);
        //} catch (RuntimeException ex) {
            //JOptionPane.showMessageDialog(null, "Taka");
        //}
        
    }//GEN-LAST:event_szczegolyActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DodawaniePracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DodawaniePracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DodawaniePracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DodawaniePracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(() -> {
            new DodawaniePracownika().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton close_button;
    private javax.swing.JTextField data_przyjecia;
    private javax.swing.JLabel data_przyjecia_label;
    private javax.swing.JTextField data_realizacji;
    private javax.swing.JLabel data_realizacji_label;
    private javax.swing.JTextField data_wysylki;
    private javax.swing.JLabel data_wysylki_label;
    private javax.swing.JTextField data_zlozenia;
    private javax.swing.JLabel data_zlozenia_label;
    private javax.swing.JTextField email;
    private javax.swing.JLabel email_label;
    private javax.swing.JTextField firma;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel nazwa_firmy_label;
    private javax.swing.JButton next_record;
    private javax.swing.JTextField platnosc;
    private javax.swing.JLabel platnosc_label;
    private javax.swing.JButton prev_record;
    private javax.swing.JTextField przedstawiciel;
    private javax.swing.JLabel przedstawiciel_label;
    private javax.swing.JTextField stan;
    private javax.swing.JLabel stan_label;
    private javax.swing.JButton szczegoly;
    private javax.swing.JTextField telefon;
    private javax.swing.JLabel telefon_label;
    // End of variables declaration//GEN-END:variables
}
