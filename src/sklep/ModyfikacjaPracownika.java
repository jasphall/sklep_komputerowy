package sklep;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ModyfikacjaPracownika extends javax.swing.JFrame {
    
    private int current_record = 0;
    
    public ModyfikacjaPracownika() {
        initComponents();
        changeValues();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        close_button = new javax.swing.JButton();
        imie_label = new javax.swing.JLabel();
        nazwisko_label = new javax.swing.JLabel();
        imie = new javax.swing.JTextField();
        nazwisko = new javax.swing.JTextField();
        stanowisko_label = new javax.swing.JLabel();
        uprawnienia_label = new javax.swing.JLabel();
        data_zatrudnienia_label = new javax.swing.JLabel();
        data_zatrudnienia = new javax.swing.JTextField();
        panel_danych_logowania = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        login = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        haslo = new javax.swing.JPasswordField();
        jLabel9 = new javax.swing.JLabel();
        haslo_confirm = new javax.swing.JPasswordField();
        data_zatrudnienia_label1 = new javax.swing.JLabel();
        wojewodztwo = new javax.swing.JTextField();
        data_zatrudnienia_label2 = new javax.swing.JLabel();
        ulica = new javax.swing.JTextField();
        data_zatrudnienia_label3 = new javax.swing.JLabel();
        nrdomu = new javax.swing.JTextField();
        data_zatrudnienia_label4 = new javax.swing.JLabel();
        nrlokalu = new javax.swing.JTextField();
        data_zatrudnienia_label5 = new javax.swing.JLabel();
        kodpocztowy = new javax.swing.JTextField();
        data_zatrudnienia_label6 = new javax.swing.JLabel();
        miejscowosc = new javax.swing.JTextField();
        modyfikuj_dane = new javax.swing.JButton();
        uprawnienia = new javax.swing.JComboBox();
        stanowisko = new javax.swing.JComboBox();
        prev_record = new javax.swing.JButton();
        next_record = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("MODYFIKACJA DANYCH PRACOWNIKA");

        close_button.setBackground(new java.awt.Color(255, 0, 0));
        close_button.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        close_button.setForeground(new java.awt.Color(255, 255, 255));
        close_button.setText("Zamknij okno");
        close_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close_buttonActionPerformed(evt);
            }
        });

        imie_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        imie_label.setText("Imię pracownika:");

        nazwisko_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        nazwisko_label.setText("Nazwisko pracownika:");

        imie.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        nazwisko.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        stanowisko_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        stanowisko_label.setText("Stanowisko:");

        uprawnienia_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        uprawnienia_label.setText("Uprawnienia:");

        data_zatrudnienia_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label.setText("Data zatrudnienia:");

        data_zatrudnienia.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        panel_danych_logowania.setBackground(new java.awt.Color(255, 255, 255));

        jLabel7.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        jLabel7.setText("Login:");

        login.setEditable(false);
        login.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        jLabel8.setText("Hasło:");

        haslo.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        jLabel9.setText("Potwierdź hasło:");

        haslo_confirm.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        haslo_confirm.setToolTipText("");

        javax.swing.GroupLayout panel_danych_logowaniaLayout = new javax.swing.GroupLayout(panel_danych_logowania);
        panel_danych_logowania.setLayout(panel_danych_logowaniaLayout);
        panel_danych_logowaniaLayout.setHorizontalGroup(
            panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_danych_logowaniaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(haslo, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
                    .addComponent(haslo_confirm)
                    .addComponent(login))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panel_danych_logowaniaLayout.setVerticalGroup(
            panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_danych_logowaniaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(haslo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(haslo_confirm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        data_zatrudnienia_label1.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label1.setText("Województwo:");

        wojewodztwo.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_zatrudnienia_label2.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label2.setText("Ulica:");

        ulica.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_zatrudnienia_label3.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label3.setText("Nr domu:");

        nrdomu.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_zatrudnienia_label4.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label4.setText("Nr lokalu:");

        nrlokalu.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_zatrudnienia_label5.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label5.setText("Kod pocztowy:");

        kodpocztowy.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_zatrudnienia_label6.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label6.setText("Miejscowość:");

        miejscowosc.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        modyfikuj_dane.setBackground(new java.awt.Color(255, 102, 0));
        modyfikuj_dane.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        modyfikuj_dane.setForeground(new java.awt.Color(255, 255, 255));
        modyfikuj_dane.setText("Modyfikuj dane");
        modyfikuj_dane.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modyfikuj_daneActionPerformed(evt);
            }
        });

        uprawnienia.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        uprawnienia.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "admin", "user" }));

        stanowisko.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        stanowisko.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Prezes", "Sprzedawca", "Specjalista", "Obsługa", "Administracja" }));

        prev_record.setBackground(new java.awt.Color(27, 161, 226));
        prev_record.setFont(new java.awt.Font("Segoe UI Light", 1, 9)); // NOI18N
        prev_record.setForeground(new java.awt.Color(255, 255, 255));
        prev_record.setText("POPRZEDNI PRACOWNIK");
        prev_record.setMargin(new java.awt.Insets(2, 0, 2, 0));
        prev_record.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prev_recordActionPerformed(evt);
            }
        });

        next_record.setBackground(new java.awt.Color(27, 161, 226));
        next_record.setFont(new java.awt.Font("Segoe UI Light", 1, 9)); // NOI18N
        next_record.setForeground(new java.awt.Color(255, 255, 255));
        next_record.setText("NASTĘPNY PRACOWNIK");
        next_record.setMargin(new java.awt.Insets(2, 0, 2, 0));
        next_record.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                next_recordActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 432, Short.MAX_VALUE)
                        .addGap(83, 83, 83)
                        .addComponent(close_button))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(uprawnienia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(uprawnienia, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(nazwisko_label, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(nazwisko, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(stanowisko_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(stanowisko, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(imie_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(imie, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(data_zatrudnienia_label6, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(miejscowosc, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(data_zatrudnienia_label4, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(nrlokalu, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(data_zatrudnienia_label5, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(kodpocztowy, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(data_zatrudnienia_label3, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(nrdomu, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(data_zatrudnienia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(data_zatrudnienia, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(data_zatrudnienia_label2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ulica, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(data_zatrudnienia_label1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(wojewodztwo, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(37, 37, 37)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(panel_danych_logowania, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(prev_record, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(next_record, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(modyfikuj_dane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(close_button)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(imie_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(imie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(nazwisko_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(nazwisko, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(stanowisko_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(stanowisko, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(15, 15, 15)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(uprawnienia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(uprawnienia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(panel_danych_logowania, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_zatrudnienia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(data_zatrudnienia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_zatrudnienia_label1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(wojewodztwo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(prev_record, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(next_record, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(data_zatrudnienia_label2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ulica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(data_zatrudnienia_label3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nrdomu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_zatrudnienia_label4, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nrlokalu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_zatrudnienia_label5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(kodpocztowy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_zatrudnienia_label6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(miejscowosc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(modyfikuj_dane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(1, 1, 1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void changeValues() {
        imie.setText(Sklep_komputerowy.pracownicy.get(current_record).getImie());
        nazwisko.setText(Sklep_komputerowy.pracownicy.get(current_record).getNazwisko());
        data_zatrudnienia.setText(Sklep_komputerowy.pracownicy.get(current_record).getDataZatrudnienia());
        wojewodztwo.setText(Sklep_komputerowy.pracownicy.get(current_record).getWojewodztwo());
        ulica.setText(Sklep_komputerowy.pracownicy.get(current_record).getUlica());
        nrdomu.setText(""+Sklep_komputerowy.pracownicy.get(current_record).getNrDomu());
        nrlokalu.setText(""+Sklep_komputerowy.pracownicy.get(current_record).getNrLokalu());
        kodpocztowy.setText(Sklep_komputerowy.pracownicy.get(current_record).getKodPocztowy());
        miejscowosc.setText(Sklep_komputerowy.pracownicy.get(current_record).getMiejcowosc());
        login.setText(Sklep_komputerowy.pracownicy.get(current_record).getLogin());
        haslo.setText(Sklep_komputerowy.pracownicy.get(current_record).getHaslo());
        haslo_confirm.setText(Sklep_komputerowy.pracownicy.get(current_record).getHaslo());
        Object o = new String();
        o = Sklep_komputerowy.pracownicy.get(current_record).getStanowisko();
        stanowisko.setSelectedItem(o);
        o = Sklep_komputerowy.pracownicy.get(current_record).getUprawnienia();
        uprawnienia.setSelectedItem(o);
    }
    
    private void setNewValues() {
        Sklep_komputerowy.pracownicy.get(current_record).setImie(imie.getText());
        Sklep_komputerowy.pracownicy.get(current_record).setNazwisko(nazwisko.getText());
        Sklep_komputerowy.pracownicy.get(current_record).setDataZatrudnienia(data_zatrudnienia.getText());
        Sklep_komputerowy.pracownicy.get(current_record).setWojewodztwo(wojewodztwo.getText());
        Sklep_komputerowy.pracownicy.get(current_record).setUlica(ulica.getText());
        Sklep_komputerowy.pracownicy.get(current_record).setNrDomu(Integer.parseInt(nrdomu.getText()));
        Sklep_komputerowy.pracownicy.get(current_record).setNrLokalu(Integer.parseInt(nrlokalu.getText()));
        Sklep_komputerowy.pracownicy.get(current_record).setKodPocztowy(kodpocztowy.getText());
        Sklep_komputerowy.pracownicy.get(current_record).setMiejscowosc(miejscowosc.getText());
        Sklep_komputerowy.pracownicy.get(current_record).setHaslo(haslo.getText());
    }
    
    private void close_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_close_buttonActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_close_buttonActionPerformed

    private void prev_recordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prev_recordActionPerformed
        if (current_record>0){
            current_record--;
            changeValues();
        }
    }//GEN-LAST:event_prev_recordActionPerformed

    private void next_recordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_next_recordActionPerformed
        if (current_record<Sklep_komputerowy.pracownicy.size()-1) current_record++;
        changeValues();
    }//GEN-LAST:event_next_recordActionPerformed

    private void modyfikuj_daneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modyfikuj_daneActionPerformed
        PreparedStatement ps;
        ResultSet rs;
        String query_pracownik = "UPDATE Pracownicy SET haslo=?, imie=?, nazwisko=?, uprawnienia=?, "
                + "stanowisko=? WHERE login=?";
        String query_adres = "UPDATE Adresy SET wojewodztwo=?, ulica=?, miejscowosc=?, kod_pocztowy=?, "
                + "nr_domu=?, nr_lokalu=? WHERE id_adresu = ?";
        Object[] options = {"Tak, jestem pewien.","Nie, muszę coś poprawić."};
        int current_id_adress = 0;
        
        if (JOptionPane.showOptionDialog(rootPane, "Czy jesteś pewien, że chcesz zmodyfikować dane tego użytkownika?", "Czy jesteś pewien?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]) ==0) {
            setNewValues();
            try {
                ps = Sklep_komputerowy.conn.prepareCall(query_pracownik);
                ps.setString(1, haslo.getText());
                ps.setString(2, imie.getText());
                ps.setString(3, nazwisko.getText());
                ps.setString(4, uprawnienia.getSelectedItem().toString());
                ps.setString(5, stanowisko.getSelectedItem().toString());
                ps.setString(6, login.getText());
                ps.executeQuery();
                
                ps = Sklep_komputerowy.conn.prepareStatement("SELECT id_adresu FROM Pracownicy where login = '"+login.getText()+"'");
                rs = ps.executeQuery();
                while (rs.next()) current_id_adress = rs.getInt("id_adresu");
                
                ps = Sklep_komputerowy.conn.prepareCall(query_adres);
                ps.setString(1, wojewodztwo.getText());
                ps.setString(2, ulica.getText());
                ps.setString(3, miejscowosc.getText());
                ps.setString(4, kodpocztowy.getText());
                ps.setString(5, nrdomu.getText());
                ps.setString(6, nrlokalu.getText());
                ps.setInt(7, current_id_adress);
                ps.executeQuery();
                
                JOptionPane.showMessageDialog(rootPane, "Udało się dokonać modyfikacji!");
                
            } catch (SQLException ex) {
                Logger.getLogger(ModyfikacjaPracownika.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_modyfikuj_daneActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DodawaniePracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DodawaniePracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DodawaniePracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DodawaniePracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(() -> {
            new DodawaniePracownika().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton close_button;
    private javax.swing.JTextField data_zatrudnienia;
    private javax.swing.JLabel data_zatrudnienia_label;
    private javax.swing.JLabel data_zatrudnienia_label1;
    private javax.swing.JLabel data_zatrudnienia_label2;
    private javax.swing.JLabel data_zatrudnienia_label3;
    private javax.swing.JLabel data_zatrudnienia_label4;
    private javax.swing.JLabel data_zatrudnienia_label5;
    private javax.swing.JLabel data_zatrudnienia_label6;
    private javax.swing.JPasswordField haslo;
    private javax.swing.JPasswordField haslo_confirm;
    private javax.swing.JTextField imie;
    private javax.swing.JLabel imie_label;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField kodpocztowy;
    private javax.swing.JTextField login;
    private javax.swing.JTextField miejscowosc;
    private javax.swing.JButton modyfikuj_dane;
    private javax.swing.JTextField nazwisko;
    private javax.swing.JLabel nazwisko_label;
    private javax.swing.JButton next_record;
    private javax.swing.JTextField nrdomu;
    private javax.swing.JTextField nrlokalu;
    private javax.swing.JPanel panel_danych_logowania;
    private javax.swing.JButton prev_record;
    private javax.swing.JComboBox stanowisko;
    private javax.swing.JLabel stanowisko_label;
    private javax.swing.JTextField ulica;
    private javax.swing.JComboBox uprawnienia;
    private javax.swing.JLabel uprawnienia_label;
    private javax.swing.JTextField wojewodztwo;
    // End of variables declaration//GEN-END:variables
}
