package sklep;

public class Zamowienie {
    private int id_zamowienia;
    private Klient k;
    private String data_zamowienia;
    private boolean czy_przyjeto_zamowienie;
    private String data_przyjecia_zamowienia;
    private String platnosc;
    private String data_wysylki;
    private boolean czy_zrealizowano;
    private String data_realizacji;

    public Zamowienie(int id_zamowienia, Klient k, String data_zamowienia, boolean czy_przyjeto_zamowienie, 
            String data_przyjecia_zamowienia, String platnosc, String data_wysylki, boolean czy_zrealizowano, 
            String data_realizacji) {
        
        this.id_zamowienia = id_zamowienia;
        this.k = k;
        this.data_zamowienia = data_zamowienia;
        this.czy_przyjeto_zamowienie = czy_przyjeto_zamowienie;
        this.data_przyjecia_zamowienia = data_przyjecia_zamowienia;
        this.platnosc = platnosc;
        this.data_wysylki = data_wysylki;
        this.czy_zrealizowano = czy_zrealizowano;
        this.data_realizacji = data_realizacji;
    }
    
    public int getId_zamowienia() {
        return id_zamowienia;
    }

    public void setId_zamowienia(int id_zamowienia) {
        this.id_zamowienia = id_zamowienia;
    }

    public Klient getK() {
        return k;
    }

    public void setK(Klient k) {
        this.k = k;
    }

    public String getData_zamowienia() {
        return data_zamowienia;
    }

    public void setData_zamowienia(String data_zamowienia) {
        this.data_zamowienia = data_zamowienia;
    }

    public boolean isCzy_przyjeto_zamowienie() {
        return czy_przyjeto_zamowienie;
    }

    public void setCzy_przyjeto_zamowienie(boolean czy_przyjeto_zamowienie) {
        this.czy_przyjeto_zamowienie = czy_przyjeto_zamowienie;
    }

    public String getData_przyjecia_zamowienia() {
        return data_przyjecia_zamowienia;
    }

    public void setData_przyjecia_zamowienia(String data_przyjecia_zamowienia) {
        this.data_przyjecia_zamowienia = data_przyjecia_zamowienia;
    }

    public String getPlatnosc() {
        return platnosc;
    }

    public void setPlatnosc(String platnosc) {
        this.platnosc = platnosc;
    }

    public String getData_wysylki() {
        return data_wysylki;
    }

    public void setData_wysylki(String data_wysylki) {
        this.data_wysylki = data_wysylki;
    }

    public boolean getCzy_zrealizowano() {
        return czy_zrealizowano;
    }

    public void setCzy_zrealizowano(boolean czy_zrealizowano) {
        this.czy_zrealizowano = czy_zrealizowano;
    }

    public String getData_realizacji() {
        return data_realizacji;
    }

    public void setData_realizacji(String data_realizacji) {
        this.data_realizacji = data_realizacji;
    }
    
    
}
