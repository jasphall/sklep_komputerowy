package sklep;

public class PanelPracownika extends javax.swing.JFrame {

    public PanelPracownika() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Panel = new javax.swing.JPanel();
        mgmt_faktury = new javax.swing.JButton();
        mgmt_produkty = new javax.swing.JButton();
        mgmt_klienci = new javax.swing.JButton();
        mgmt_zamowienia = new javax.swing.JButton();
        mgmt_pracownicy = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));

        Panel.setBackground(new java.awt.Color(255, 255, 255));

        mgmt_faktury.setBackground(new java.awt.Color(250, 104, 0));
        mgmt_faktury.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        mgmt_faktury.setForeground(new java.awt.Color(255, 255, 255));
        mgmt_faktury.setText("Zarządzanie fakturami");
        mgmt_faktury.setBorder(null);
        mgmt_faktury.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        mgmt_produkty.setBackground(new java.awt.Color(118, 96, 138));
        mgmt_produkty.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        mgmt_produkty.setForeground(new java.awt.Color(255, 255, 255));
        mgmt_produkty.setText("Zarządzanie produktami");
        mgmt_produkty.setBorder(null);
        mgmt_produkty.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        mgmt_produkty.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mgmt_produktyActionPerformed(evt);
            }
        });

        mgmt_klienci.setBackground(new java.awt.Color(0, 138, 0));
        mgmt_klienci.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        mgmt_klienci.setForeground(new java.awt.Color(255, 255, 255));
        mgmt_klienci.setText("Zarządzanie klientami");
        mgmt_klienci.setBorder(null);
        mgmt_klienci.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        mgmt_klienci.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mgmt_klienciActionPerformed(evt);
            }
        });

        mgmt_zamowienia.setBackground(new java.awt.Color(122, 59, 63));
        mgmt_zamowienia.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        mgmt_zamowienia.setForeground(new java.awt.Color(255, 255, 255));
        mgmt_zamowienia.setText("Zarządzanie zamówieniami");
        mgmt_zamowienia.setBorder(null);
        mgmt_zamowienia.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        mgmt_zamowienia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mgmt_zamowieniaActionPerformed(evt);
            }
        });

        mgmt_pracownicy.setBackground(new java.awt.Color(27, 161, 226));
        mgmt_pracownicy.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        mgmt_pracownicy.setForeground(new java.awt.Color(255, 255, 255));
        mgmt_pracownicy.setText("Zarządzanie pracownikami");
        mgmt_pracownicy.setBorder(null);
        mgmt_pracownicy.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        mgmt_pracownicy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mgmt_pracownicyActionPerformed(evt);
            }
        });

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Segoe UI Light", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(27, 161, 226));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("PANEL PRACOWNIKA");

        javax.swing.GroupLayout PanelLayout = new javax.swing.GroupLayout(Panel);
        Panel.setLayout(PanelLayout);
        PanelLayout.setHorizontalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mgmt_faktury, javax.swing.GroupLayout.PREFERRED_SIZE, 578, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(mgmt_produkty, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(mgmt_zamowienia, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(mgmt_pracownicy, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(mgmt_klienci, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(24, Short.MAX_VALUE))
            .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PanelLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(5, 5, 5)))
        );
        PanelLayout.setVerticalGroup(
            PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PanelLayout.createSequentialGroup()
                .addContainerGap(68, Short.MAX_VALUE)
                .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelLayout.createSequentialGroup()
                        .addComponent(mgmt_pracownicy, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mgmt_produkty, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(mgmt_zamowienia, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(mgmt_klienci, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(mgmt_faktury, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52))
            .addGroup(PanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(PanelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(395, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(Panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 2, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void mgmt_pracownicyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mgmt_pracownicyActionPerformed
        sklep.ZarzadzaniePracownikami panel_zp = new sklep.ZarzadzaniePracownikami();
        panel_zp.setVisible(true);
    }//GEN-LAST:event_mgmt_pracownicyActionPerformed

    private void mgmt_klienciActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mgmt_klienciActionPerformed
        sklep.ZarzadzanieKlientami panel_zk = new sklep.ZarzadzanieKlientami();
        panel_zk.setVisible(true);
    }//GEN-LAST:event_mgmt_klienciActionPerformed

    private void mgmt_produktyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mgmt_produktyActionPerformed
        sklep.ZarzadzanieProduktami panel_zp = new sklep.ZarzadzanieProduktami();
        panel_zp.setVisible(true);
    }//GEN-LAST:event_mgmt_produktyActionPerformed

    private void mgmt_zamowieniaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mgmt_zamowieniaActionPerformed
        sklep.ZarzadzanieZamowieniami zz = new ZarzadzanieZamowieniami();
        zz.setVisible(true);
    }//GEN-LAST:event_mgmt_zamowieniaActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PanelPracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PanelPracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PanelPracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PanelPracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PanelPracownika().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Panel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton mgmt_faktury;
    private javax.swing.JButton mgmt_klienci;
    private javax.swing.JButton mgmt_pracownicy;
    private javax.swing.JButton mgmt_produkty;
    private javax.swing.JButton mgmt_zamowienia;
    // End of variables declaration//GEN-END:variables
}
