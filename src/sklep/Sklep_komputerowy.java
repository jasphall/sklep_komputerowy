package sklep;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

public class Sklep_komputerowy {
    final static int IS_DEV =0;    //  0-pracownik, 1-klient, -1-tryb wylaczony
    public static Connection conn = null;
    private static ResultSet rs = null;
    public static List<Pracownik> pracownicy = new ArrayList<>();
    public static List<Klient> klienci = new ArrayList<>();
    public static List<Produkt> produkty = new ArrayList<>();
    public static List<Zamowienie> zamowienia = new ArrayList<>();
    public static List<ProduktZamowienie> prod_zam = new ArrayList<>();
    
    public static int autoryzacjaUzytkownika(String login, String haslo){
        //zwraca 0, gdy zalogowal sie pracownik, 1, gdy klient, -1 gdy nie ma takiego uzytkownika
        try {
            Statement stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT login,haslo FROM Pracownicy");
            while (rs.next()){
                if (login.equals(rs.getString("LOGIN"))){
                    if (haslo.equals(rs.getString("HASLO"))){
                        return 0;
                    }
                }
            }
            rs = stmt.executeQuery("SELECT login, haslo FROM Klienci");
            while (rs.next()){
                if (login.equals(rs.getString("LOGIN"))){
                    if (haslo.equals(rs.getString("HASLO"))){
                        return 1;
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Sklep_komputerowy.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
    public static void wczytajPracownikow(){
        String[] dane = new String[12];
        int[] dane_int = new int[3];
    
        pracownicy.clear();
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("SELECT * FROM Pracownicy, Adresy WHERE Adresy.ID_ADRESU = Pracownicy.ID_ADRESU ORDER BY id_pracownika ASC");
            while (rs.next()){
                dane[0] = rs.getString("IMIE");
                dane[1] = rs.getString("NAZWISKO");
                dane[2] = rs.getString("LOGIN");
                dane[3] = rs.getString("HASLO");
                dane[4] = rs.getString("WOJEWODZTWO");
                dane[5] = rs.getString("ULICA");
                dane[6] = rs.getString("MIEJSCOWOSC");
                dane[7] = rs.getString("KOD_POCZTOWY");
                dane_int[0] = rs.getInt("NR_DOMU");
                dane_int[1] = rs.getInt("NR_LOKALU");
                dane_int[2] = rs.getInt("ID_PRACOWNIKA");
                dane[8] = rs.getString("UPRAWNIENIA");
                dane[9] = rs.getString("KONTO_AKTYWNE");
                dane[10] = rs.getString("DATA_ZATRUDNIENIA");
                dane[11] = rs.getString("STANOWISKO");
               
                pracownicy.add(new Pracownik(dane_int[2], dane[8], dane[9], dane[10], dane[11], dane[0], dane[1], dane[2], 
                        dane[3], dane[4], dane[5], dane[6], dane[7], dane_int[0], dane_int[1]));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Sklep_komputerowy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void wczytajKlientow(){
        String[] dane = new String[13];
        int[] dane_int = new int[3];
    
        klienci.clear();
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("SELECT * FROM Klienci, Adresy WHERE Adresy.ID_ADRESU = Klienci.ID_ADRESU ORDER BY id_klienta ASC");
            
            while (rs.next()){
                dane[0] = rs.getString("NAZWISKO");
                dane[1] = rs.getString("IMIE");
                dane[2] = rs.getString("LOGIN");
                dane[3] = rs.getString("HASLO");
                dane[4] = rs.getString("NAZWA_FIRMY");
                dane[5] = rs.getString("REGON");
                dane[6] = rs.getString("NIP");
                dane[7] = rs.getString("TELEFON");
                dane[8] = rs.getString("EMAIL");
                dane[9] = rs.getString("WOJEWODZTWO");
                dane[10] = rs.getString("ULICA");
                dane[11] = rs.getString("MIEJSCOWOSC");
                dane[12] = rs.getString("KOD_POCZTOWY");
                dane_int[0] = rs.getInt("NR_DOMU");
                dane_int[1] = rs.getInt("NR_LOKALU");
                dane_int[2] = rs.getInt("ID_KLIENTA");
               
                
                klienci.add(new Klient(dane_int[2], dane[4], dane[5], dane[6], dane[7], dane[8], dane[1], dane[0], 
                        dane[2], dane[3], dane[9], dane[10], dane[11], dane[12], dane_int[0], dane_int[1]));
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(Sklep_komputerowy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void wczytajProdukty(){
        String[] dane = new String[9];
        int[] dane_int = new int[3];
        float[] dane_ceny = new float[2];
        Blob img;
        
    
        produkty.clear();
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("SELECT * FROM Produkty, Producenci WHERE Produkty.ID_PRODUCENTA = Producenci.ID_PRODUCENTA ORDER BY id_produktu");
            
            while (rs.next()){
                dane[0] = rs.getString("NAZWA_PRODUKTU");
                dane[1] = rs.getString("TYP");
                dane[2] = rs.getString("NR_SERYJNY");
                dane[3] = rs.getString("OPIS");
                dane[4] = rs.getString("NAZWA");    //  producenta
                dane[5] = rs.getString("ADRES");
                dane[6] = rs.getString("EMAIL");
                dane[7] = rs.getString("TELEFON");
                dane[8] = rs.getString("OSOBA_KONTAKTOWA");
                dane_int[0] = rs.getInt("ID_PRODUKTU");
                dane_int[1] = rs.getInt("ID_PRODUCENTA");
                dane_int[2] = rs.getInt("ILOSC_DOST_SZTUK");
                dane_ceny[0] = rs.getFloat("CENA_BRUTTO");
                dane_ceny[1] = rs.getFloat("CENA_NETTO");
                img = rs.getBlob("FOTOGRAFIA");
               
                produkty.add(new Produkt(dane_int[0], dane[0], dane[1], dane[2], dane[3], img, dane_ceny[0], dane_ceny[1], 
                        dane_int[2], new Producent(dane_int[1], dane[4], dane[5], dane[7], dane[6], dane[8])));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Sklep_komputerowy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void wczytajZamowienia(){
        String[] dane_str = new String[14];
        int[] dane_int = new int[5]; 
        boolean[] dane_bool = new boolean[2];
        Klient klient = null;
    
        zamowienia.clear();
        try {
            Statement s = conn.createStatement();
            rs = s.executeQuery("SELECT * FROM Zamowienia, Klienci WHERE Zamowienia.ID_KLIENTA = Klienci.ID_KLIENTA ORDER BY ID_ZAMOWIENIA");
            
            while (rs.next()){
                dane_int[0] = rs.getInt("ID_ZAMOWIENIA");
                dane_str[0] = rs.getString("DATA_ZLOZENIA_ZAMOWIENIA");
                dane_bool[0] = rs.getBoolean("CZY_PRZYJETO_ZAMOWIENIE");
                dane_str[1] = rs.getString("DATA_PRZYJECIA_ZAMOWIENIA");
                dane_str[2] = rs.getString("PLATNOSC");
                dane_str[3] = rs.getString("DATA_WYSYLKI");
                dane_bool[1] = rs.getBoolean("CZY_ZREALIZOWANO");
                dane_str[4] = rs.getString("DATA_REALIZACJI");
                dane_int[3] = rs.getInt("ID_KLIENTA");
                dane_int[4] = rs.getInt("ID_ADRESU");
                dane_str[5] = rs.getString("NAZWISKO");
                dane_str[6] = rs.getString("IMIE");
                dane_str[7] = rs.getString("LOGIN");
                dane_str[8] = rs.getString("HASLO");
                dane_str[9] = rs.getString("NAZWA_FIRMY");
                dane_str[10] = rs.getString("REGON");
                dane_str[11] = rs.getString("NIP");
                dane_str[12] = rs.getString("TELEFON");
                dane_str[13] = rs.getString("EMAIL");
               
                if (dane_str[0] != null) dane_str[0] = dane_str[0].substring(0, 10);
                if (dane_str[1] != null) dane_str[1] = dane_str[1].substring(0, 10);
                if (dane_str[3] != null) dane_str[3] = dane_str[3].substring(0, 10);
                if (dane_str[4] != null) dane_str[4] = dane_str[4].substring(0, 10);
                
                for (Klient k : klienci) {
                    if (k.getIdKlienta() == dane_int[3]) {
                        klient = k;
                    }
                }
                
                zamowienia.add(new Zamowienie(dane_int[0], klient, dane_str[0], dane_bool[0], 
                    dane_str[1], dane_str[2], dane_str[3], dane_bool[1], dane_str[4]));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Sklep_komputerowy.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void wczytajProduktyZamowienia() {
        PreparedStatement ps = null;
        String query = "select z.id_zamowienia, p.id_produktu, z.id_klienta, zp.ilosc_sztuk from produkty p, "
                + "zamowienia_produkty zp, zamowienia z where p.id_produktu = zp.id_produktu and "
                + "zp.id_zamowienia = z.id_zamowienia" 
                + " order by z.id_zamowienia, p.id_produktu, z.id_klienta, zp.ilosc_sztuk";
        
        try {
            ps = Sklep_komputerowy.conn.prepareCall(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                prod_zam.add(new ProduktZamowienie(rs.getInt("ID_PRODUKTU"), 
                        rs.getInt("ID_KLIENTA"), rs.getInt("ILOSC_SZTUK")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SzczegolyZamowienia.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void wczytajBaze(){
        wczytajPracownikow();
        wczytajKlientow();
        wczytajProdukty();
        wczytajZamowienia();
        wczytajProduktyZamowienia();
    }
    
    public static void main(String[] args) {
        PanelLogowania log = new PanelLogowania();
        PanelKlienta pk = new PanelKlienta();
        PanelPracownika pp = new PanelPracownika();
        
        conn = ConnectionManager.connectToDatabase();
        
        wczytajBaze();
        
        if (IS_DEV==-1) log.setVisible(true);
        else if (IS_DEV==0) pp.setVisible(true);
        else pk.setVisible(true);
        
        //OknoGlowne okno = new OknoGlowne();
        //okno.setVisible(true);
    }
    
}
