package sklep;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ZarzadzanieKlientami extends javax.swing.JFrame {
    
    int current_record = 0;
    
    public ZarzadzanieKlientami() {
        initComponents();
        changeValues();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        pole_imie_nazwisko = new javax.swing.JTextField();
        prev_record = new javax.swing.JButton();
        next_record = new javax.swing.JButton();
        pole_firma = new javax.swing.JTextField();
        pole_dane_adresowe = new javax.swing.JTextField();
        pole_kontakt = new javax.swing.JTextField();
        separator = new javax.swing.JSeparator();
        dodaj_pracownika = new javax.swing.JButton();
        usun_pracownika = new javax.swing.JButton();
        zamknij_okno = new javax.swing.JButton();
        odswiez = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Segoe UI Light", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("ZARZĄDZANIE KLIENTAMI");

        pole_imie_nazwisko.setEditable(false);
        pole_imie_nazwisko.setFont(new java.awt.Font("Segoe UI Light", 1, 14)); // NOI18N
        pole_imie_nazwisko.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        prev_record.setBackground(new java.awt.Color(27, 161, 226));
        prev_record.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        prev_record.setForeground(new java.awt.Color(255, 255, 255));
        prev_record.setText("POPRZEDNI KLIENT");
        prev_record.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prev_recordActionPerformed(evt);
            }
        });

        next_record.setBackground(new java.awt.Color(27, 161, 226));
        next_record.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        next_record.setForeground(new java.awt.Color(255, 255, 255));
        next_record.setText("NASTĘPNY KLIENT");
        next_record.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                next_recordActionPerformed(evt);
            }
        });

        pole_firma.setEditable(false);
        pole_firma.setFont(new java.awt.Font("Segoe UI Light", 0, 10)); // NOI18N
        pole_firma.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        pole_dane_adresowe.setEditable(false);
        pole_dane_adresowe.setFont(new java.awt.Font("Segoe UI Light", 0, 12)); // NOI18N
        pole_dane_adresowe.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        pole_kontakt.setEditable(false);
        pole_kontakt.setFont(new java.awt.Font("Segoe UI Light", 0, 10)); // NOI18N
        pole_kontakt.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        dodaj_pracownika.setBackground(new java.awt.Color(250, 104, 0));
        dodaj_pracownika.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        dodaj_pracownika.setForeground(new java.awt.Color(255, 255, 255));
        dodaj_pracownika.setText("DODAJ KLIENTA");
        dodaj_pracownika.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dodaj_pracownikaActionPerformed(evt);
            }
        });

        usun_pracownika.setBackground(new java.awt.Color(250, 104, 0));
        usun_pracownika.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        usun_pracownika.setForeground(new java.awt.Color(255, 255, 255));
        usun_pracownika.setText("USUŃ KLIENTA");
        usun_pracownika.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usun_pracownikaActionPerformed(evt);
            }
        });

        zamknij_okno.setBackground(new java.awt.Color(255, 51, 51));
        zamknij_okno.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        zamknij_okno.setForeground(new java.awt.Color(255, 255, 255));
        zamknij_okno.setText("Zamknij okno");
        zamknij_okno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zamknij_oknoActionPerformed(evt);
            }
        });

        odswiez.setBackground(new java.awt.Color(27, 161, 226));
        odswiez.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        odswiez.setForeground(new java.awt.Color(255, 255, 255));
        odswiez.setText("Odśwież");
        odswiez.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                odswiezActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 426, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(odswiez, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(zamknij_okno))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(dodaj_pracownika, javax.swing.GroupLayout.PREFERRED_SIZE, 301, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(usun_pracownika, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pole_imie_nazwisko, javax.swing.GroupLayout.DEFAULT_SIZE, 373, Short.MAX_VALUE)
                            .addComponent(pole_firma))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pole_dane_adresowe)
                            .addComponent(pole_kontakt)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(prev_record, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(next_record, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(separator))
                .addGap(10, 10, 10))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(zamknij_okno)
                        .addComponent(odswiez)))
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pole_imie_nazwisko, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pole_dane_adresowe, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pole_firma, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE)
                    .addComponent(pole_kontakt))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(prev_record, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(next_record, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(separator, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(dodaj_pracownika, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                    .addComponent(usun_pracownika, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void changeValues() {
        pole_imie_nazwisko.setText(Sklep_komputerowy.klienci.get(current_record).getImie()
                + " " + Sklep_komputerowy.klienci.get(current_record).getNazwisko() 
                + " (" + Sklep_komputerowy.klienci.get(current_record).getLogin() + ")");
        pole_firma.setText(Sklep_komputerowy.klienci.get(current_record).getNazwaFirmy() 
                + " (NIP: " + Sklep_komputerowy.klienci.get(current_record).getNip() + " / REGON: " 
                + Sklep_komputerowy.klienci.get(current_record).getRegon()+ ")");
        pole_kontakt.setText("Telefon: " + Sklep_komputerowy.klienci.get(current_record).getTelefon() 
                + "\nE-mail: " + Sklep_komputerowy.klienci.get(current_record).getEmail());
        pole_dane_adresowe.setText("ul. " + Sklep_komputerowy.klienci.get(current_record).getUlica() 
                + " " + Sklep_komputerowy.klienci.get(current_record).getNrDomu() + "/" 
                + Sklep_komputerowy.klienci.get(current_record).getNrLokalu()+ ", " 
                + Sklep_komputerowy.klienci.get(current_record).getKodPocztowy() + " " 
                + Sklep_komputerowy.klienci.get(current_record).getMiejcowosc());
    }
    
    private void next_recordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_next_recordActionPerformed
        if (current_record<Sklep_komputerowy.klienci.size()-1) current_record++;
        changeValues();
    }//GEN-LAST:event_next_recordActionPerformed

    private void prev_recordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prev_recordActionPerformed
        if (current_record>0){
            current_record--;
            changeValues();
        }
    }//GEN-LAST:event_prev_recordActionPerformed

    private void zamknij_oknoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zamknij_oknoActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_zamknij_oknoActionPerformed

    private void dodaj_pracownikaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dodaj_pracownikaActionPerformed
        DodawanieKlienta panel_dodawania_klienta = new DodawanieKlienta();
        panel_dodawania_klienta.setVisible(true);
    }//GEN-LAST:event_dodaj_pracownikaActionPerformed

    private void odswiezActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_odswiezActionPerformed
        Sklep_komputerowy.wczytajKlientow();
        JOptionPane.showMessageDialog(rootPane, "Baza została zaktualizowana!");
    }//GEN-LAST:event_odswiezActionPerformed

    private void usun_pracownikaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usun_pracownikaActionPerformed
        Object[] prac = new Object[Sklep_komputerowy.klienci.size()];
        PreparedStatement ps;
        ResultSet rs;
        String query_klienci;
        String query_adresy;
        int k=0;
        int nr_adresu = 0;
        
        for (int i=0; i<Sklep_komputerowy.klienci.size(); i++){
            prac[i] = "" + Sklep_komputerowy.klienci.get(i).imie
            + " " + Sklep_komputerowy.klienci.get(i).nazwisko + " [" 
            + Sklep_komputerowy.klienci.get(i).login + "]";
        }
        
        String p = (String)JOptionPane.showInputDialog(null, "Wybierz klienta, którego chcesz usunąć: ", "Usuwanie klienta",
            JOptionPane.QUESTION_MESSAGE, null, prac, 0);
       
        while (p.charAt(k) != '[') k++;
        String w = p.substring(k+1,p.length()-1);
        
        try {
            ps = Sklep_komputerowy.conn.prepareCall("SELECT * FROM Klienci where login = '" + w + "'");
            rs = ps.executeQuery();
            while (rs.next()) {
                nr_adresu = rs.getInt("id_adresu");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ZarzadzanieKlientami.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for (int i=0; i<Sklep_komputerowy.klienci.size(); i++) {
            if (Sklep_komputerowy.klienci.get(i).login.equals(w)) {
                Sklep_komputerowy.klienci.remove(Sklep_komputerowy.klienci.get(i));
            }
        }
        
        try {
            query_klienci = "DELETE FROM Klienci WHERE login = '" + w + "'";
            query_adresy = "DELETE FROM Adresy WHERE id_adresu = " + nr_adresu;
            
            ps = Sklep_komputerowy.conn.prepareCall(query_klienci);
            ps.executeQuery();
            ps = Sklep_komputerowy.conn.prepareCall(query_adresy);
            ps.executeQuery();
            
        } catch (SQLException ex) {
            Logger.getLogger(ZarzadzanieKlientami.class.getName()).log(Level.SEVERE, null, ex);
        }
        JOptionPane.showMessageDialog(rootPane, "Usunieto klienta z bazy!");
    }//GEN-LAST:event_usun_pracownikaActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ZarzadzanieKlientami.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ZarzadzanieKlientami.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ZarzadzanieKlientami.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ZarzadzanieKlientami.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ZarzadzanieKlientami().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton dodaj_pracownika;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JButton next_record;
    private javax.swing.JButton odswiez;
    private static javax.swing.JTextField pole_dane_adresowe;
    private static javax.swing.JTextField pole_firma;
    private static javax.swing.JTextField pole_imie_nazwisko;
    private static javax.swing.JTextField pole_kontakt;
    private javax.swing.JButton prev_record;
    private javax.swing.JSeparator separator;
    private javax.swing.JButton usun_pracownika;
    private javax.swing.JButton zamknij_okno;
    // End of variables declaration//GEN-END:variables
}
