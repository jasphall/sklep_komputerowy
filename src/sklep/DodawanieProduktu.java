package sklep;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class DodawanieProduktu extends javax.swing.JFrame {
    
    public DodawanieProduktu() {
        initComponents();
        clear();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label_tytul = new javax.swing.JLabel();
        close_button = new javax.swing.JButton();
        nazwa_label = new javax.swing.JLabel();
        typ_label = new javax.swing.JLabel();
        nazwa = new javax.swing.JTextField();
        typ = new javax.swing.JTextField();
        data_zatrudnienia_label1 = new javax.swing.JLabel();
        netto_label = new javax.swing.JLabel();
        netto = new javax.swing.JTextField();
        brutto_label = new javax.swing.JLabel();
        brutto = new javax.swing.JTextField();
        dostepne_label = new javax.swing.JLabel();
        dostepne = new javax.swing.JTextField();
        producent_label = new javax.swing.JLabel();
        producent = new javax.swing.JTextField();
        dodaj_produkt = new javax.swing.JButton();
        wyczysc = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        opis = new javax.swing.JTextArea();
        nr_seryjny_label = new javax.swing.JLabel();
        nr_seryjny = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        label_tytul.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        label_tytul.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_tytul.setText("DODAWANIE NOWEGO PRODUKTU");

        close_button.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        close_button.setText("Zamknij okno");
        close_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close_buttonActionPerformed(evt);
            }
        });

        nazwa_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        nazwa_label.setText("Nazwa produktu:");

        typ_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        typ_label.setText("Typ produktu:");

        nazwa.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        typ.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_zatrudnienia_label1.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label1.setText("Opis:");

        netto_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        netto_label.setText("Cena netto:");

        netto.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        brutto_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        brutto_label.setText("Cena brutto:");

        brutto.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        dostepne_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        dostepne_label.setText("Ilość sztuk dostępnych:");

        dostepne.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        producent_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        producent_label.setText("Producent:");

        producent.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        dodaj_produkt.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        dodaj_produkt.setText("Dodaj produkt");
        dodaj_produkt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dodaj_produktActionPerformed(evt);
            }
        });

        wyczysc.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        wyczysc.setText("Wyczyść");
        wyczysc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wyczyscActionPerformed(evt);
            }
        });

        opis.setColumns(20);
        opis.setRows(5);
        jScrollPane1.setViewportView(opis);
        opis.setLineWrap(true);

        nr_seryjny_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        nr_seryjny_label.setText("Nr seryjny: ");

        nr_seryjny.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(label_tytul, javax.swing.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
                        .addGap(83, 83, 83)
                        .addComponent(close_button)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(typ_label, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(typ, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(nazwa_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(nazwa, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(data_zatrudnienia_label1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(netto_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(netto, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(dostepne_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dostepne, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(brutto_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(brutto, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(37, 37, 37)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(dodaj_produkt, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(wyczysc, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(producent_label, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(producent, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(nr_seryjny_label, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nr_seryjny, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap())))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 527, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(close_button)
                    .addComponent(label_tytul, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nazwa_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nazwa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(typ_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(typ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(producent_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(producent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nr_seryjny_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nr_seryjny, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(3, 3, 3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(data_zatrudnienia_label1, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(wyczysc)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dodaj_produkt, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(netto_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(netto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(brutto_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(brutto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(dostepne_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dostepne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void close_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_close_buttonActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_close_buttonActionPerformed

    private void clear() {
        nazwa.setText(null);
        typ.setText(null);
        producent.setText(null);
        nr_seryjny.setText(null);
        opis.setText(null);
        netto.setText(null);
        brutto.setText(null);
        dostepne.setText(null);
    }
    
    private void wyczyscActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wyczyscActionPerformed
        clear();
    }//GEN-LAST:event_wyczyscActionPerformed

    private void dodaj_produktActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dodaj_produktActionPerformed
        PreparedStatement ps;
        ResultSet rs;
        String query_produkty = "INSERT INTO PRODUKTY (ID_PRODUKTU, NAZWA_PRODUKTU, ID_PRODUCENTA, TYP, "
                + "NR_SERYJNY, OPIS, FOTOGRAFIA, CENA_BRUTTO, CENA_NETTO, ILOSC_DOST_SZTUK) " 
                + "VALUES (?, ?, ?, ?, ?, ?, null, ?, ?, ?)";
        String query_producenci = "SELECT ID_PRODUCENTA FROM PRODUCENCI WHERE nazwa = '" + producent.getText() + "'";
        Object[] options = {"Tak, jestem pewien.","Nie, muszę coś poprawić."};
        int current_max_produkt_id = Sklep_komputerowy.produkty.get(Sklep_komputerowy.produkty.size()-1).getIdProduktu();
        int id_producent = -1;
        
        if (JOptionPane.showOptionDialog(rootPane, "Czy jesteś pewien, że chcesz dodać nowy produkt?", "Czy jesteś pewien?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]) ==0)
        {
            try {
                
                current_max_produkt_id++;
                
                ps = Sklep_komputerowy.conn.prepareCall(query_producenci);
                rs = ps.executeQuery();
                if (rs.next()) id_producent = rs.getInt("ID_PRODUCENTA");
                
                ps = Sklep_komputerowy.conn.prepareCall(query_produkty);
                ps.setInt(1, current_max_produkt_id);
                ps.setString(2, nazwa.getText());
                ps.setInt(3, id_producent);
                ps.setString(4, typ.getText());
                ps.setString(5, nr_seryjny.getText());
                ps.setString(6, opis.getText());
                ps.setFloat(7, Integer.parseInt(brutto.getText()));
                ps.setFloat(8, Integer.parseInt(netto.getText()));
                ps.setInt(9, Integer.parseInt(dostepne.getText()));
                ps.executeQuery();
                
                JOptionPane.showMessageDialog(rootPane, "Dodano nowy produkt!");
            } catch (SQLException ex) {
                Logger.getLogger(DodawanieProduktu.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Sklep_komputerowy.wczytajProdukty();
    }//GEN-LAST:event_dodaj_produktActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    try {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(DodawanieProduktu.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IllegalAccessException ex) {
                        Logger.getLogger(DodawanieProduktu.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                }
            }
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DodawanieProduktu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DodawanieProduktu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DodawanieProduktu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField brutto;
    private javax.swing.JLabel brutto_label;
    private javax.swing.JButton close_button;
    private javax.swing.JLabel data_zatrudnienia_label1;
    private javax.swing.JButton dodaj_produkt;
    private javax.swing.JTextField dostepne;
    private javax.swing.JLabel dostepne_label;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel label_tytul;
    private javax.swing.JTextField nazwa;
    private javax.swing.JLabel nazwa_label;
    private javax.swing.JTextField netto;
    private javax.swing.JLabel netto_label;
    private javax.swing.JTextField nr_seryjny;
    private javax.swing.JLabel nr_seryjny_label;
    private javax.swing.JTextArea opis;
    private javax.swing.JTextField producent;
    private javax.swing.JLabel producent_label;
    private javax.swing.JTextField typ;
    private javax.swing.JLabel typ_label;
    private javax.swing.JButton wyczysc;
    // End of variables declaration//GEN-END:variables
}
