package sklep;

public final class Producent {
    private int id_producenta;
    private String nazwa_producenta;
    private String adres;
    private String telefon;
    private String email;
    private String osoba_kontaktowa;
    
    public int getIdProducenta(){
        return id_producenta;
    }
    
    public void setIdProducenta(int id){
        this.id_producenta = id;
    }
    
    public String getNazwaProducenta(){
        return nazwa_producenta;
    }
    
    public void setNazwaProducenta(String nazwa_producenta){
        this.nazwa_producenta = nazwa_producenta;
    }
    
    public String getAdres(){
        return adres;
    }
    
    public void setAdres(String adres){
        this.adres = adres;
    }
    
    public String getTelefon(){
        return telefon;
    }
    
    public void setTelefon(String telefon){
        this.telefon = telefon;
    }
    
    public String getEmail(){
        return email;
    }
    
    public void setEmail(String email){
        this.email = email;
    }
    
    public String getOsobaKontaktowa(){
        return osoba_kontaktowa;
    }
    
    public void setOsobaKontaktowa(String osoba){
        this.osoba_kontaktowa = osoba;
    }

    public Producent(int id_producenta, String nazwa_producenta, String adres, String telefon, String email, String osoba_kontaktowa) {
        this.id_producenta = id_producenta;
        this.nazwa_producenta = nazwa_producenta;
        this.adres = adres;
        this.telefon = telefon;
        this.email = email;
        this.osoba_kontaktowa = osoba_kontaktowa;
    }
}
