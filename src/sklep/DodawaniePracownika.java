package sklep;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Timestamp;
import javax.swing.JOptionPane;

public class DodawaniePracownika extends javax.swing.JFrame {
    
    public DodawaniePracownika() {
        initComponents();
        clear();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label_tytul = new javax.swing.JLabel();
        close_button = new javax.swing.JButton();
        imie_label = new javax.swing.JLabel();
        nazwisko_label = new javax.swing.JLabel();
        imie = new javax.swing.JTextField();
        nazwisko = new javax.swing.JTextField();
        stanowisko_label = new javax.swing.JLabel();
        uprawnienia_label = new javax.swing.JLabel();
        data_zatrudnienia_label = new javax.swing.JLabel();
        data_zatrudnienia = new javax.swing.JTextField();
        panel_danych_logowania = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        login = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        haslo = new javax.swing.JPasswordField();
        jLabel9 = new javax.swing.JLabel();
        haslo_confirm = new javax.swing.JPasswordField();
        data_zatrudnienia_label1 = new javax.swing.JLabel();
        wojewodztwo = new javax.swing.JTextField();
        data_zatrudnienia_label2 = new javax.swing.JLabel();
        ulica = new javax.swing.JTextField();
        data_zatrudnienia_label3 = new javax.swing.JLabel();
        nrdomu = new javax.swing.JTextField();
        data_zatrudnienia_label4 = new javax.swing.JLabel();
        nrlokalu = new javax.swing.JTextField();
        data_zatrudnienia_label5 = new javax.swing.JLabel();
        kodpocztowy = new javax.swing.JTextField();
        data_zatrudnienia_label6 = new javax.swing.JLabel();
        miejscowosc = new javax.swing.JTextField();
        dodaj_pracownika = new javax.swing.JButton();
        wyczysc = new javax.swing.JButton();
        uprawnienia = new javax.swing.JComboBox();
        stanowisko = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        label_tytul.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        label_tytul.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_tytul.setText("DODAWANIE NOWEGO PRACOWNIKA");

        close_button.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        close_button.setText("Zamknij okno");
        close_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close_buttonActionPerformed(evt);
            }
        });

        imie_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        imie_label.setText("Imię pracownika:");

        nazwisko_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        nazwisko_label.setText("Nazwisko pracownika:");

        imie.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        nazwisko.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        stanowisko_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        stanowisko_label.setText("Stanowisko:");

        uprawnienia_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        uprawnienia_label.setText("Uprawnienia:");

        data_zatrudnienia_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label.setText("Data zatrudnienia:");

        data_zatrudnienia.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        panel_danych_logowania.setBackground(new java.awt.Color(255, 255, 255));

        jLabel7.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        jLabel7.setText("Login:");

        login.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        jLabel8.setText("Hasło:");

        haslo.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        jLabel9.setText("Potwierdź hasło:");

        haslo_confirm.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        haslo_confirm.setToolTipText("");

        javax.swing.GroupLayout panel_danych_logowaniaLayout = new javax.swing.GroupLayout(panel_danych_logowania);
        panel_danych_logowania.setLayout(panel_danych_logowaniaLayout);
        panel_danych_logowaniaLayout.setHorizontalGroup(
            panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_danych_logowaniaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(haslo, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
                    .addComponent(haslo_confirm)
                    .addComponent(login))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        panel_danych_logowaniaLayout.setVerticalGroup(
            panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_danych_logowaniaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(haslo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(haslo_confirm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        data_zatrudnienia_label1.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label1.setText("Województwo:");

        wojewodztwo.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_zatrudnienia_label2.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label2.setText("Ulica:");

        ulica.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_zatrudnienia_label3.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label3.setText("Nr domu:");

        nrdomu.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_zatrudnienia_label4.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label4.setText("Nr lokalu:");

        nrlokalu.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_zatrudnienia_label5.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label5.setText("Kod pocztowy:");

        kodpocztowy.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_zatrudnienia_label6.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label6.setText("Miejscowość:");

        miejscowosc.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        dodaj_pracownika.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        dodaj_pracownika.setText("Dodaj pracownika");
        dodaj_pracownika.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dodaj_pracownikaActionPerformed(evt);
            }
        });

        wyczysc.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        wyczysc.setText("Wyczyść");
        wyczysc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wyczyscActionPerformed(evt);
            }
        });

        uprawnienia.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        uprawnienia.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "admin", "user" }));

        stanowisko.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        stanowisko.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Sprzedawca", "Specjalista", "Obsługa", "Administracja" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(label_tytul, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
                        .addGap(83, 83, 83)
                        .addComponent(close_button))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(data_zatrudnienia_label1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(wojewodztwo, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(data_zatrudnienia_label2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ulica, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(data_zatrudnienia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(data_zatrudnienia, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(uprawnienia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(uprawnienia, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(nazwisko_label, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nazwisko, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(stanowisko_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(stanowisko, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(imie_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(imie, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(data_zatrudnienia_label6, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(miejscowosc, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(data_zatrudnienia_label4, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nrlokalu, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(data_zatrudnienia_label5, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(kodpocztowy, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(data_zatrudnienia_label3, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nrdomu, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(37, 37, 37)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(panel_danych_logowania, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(dodaj_pracownika, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(wyczysc, javax.swing.GroupLayout.Alignment.TRAILING))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(close_button)
                    .addComponent(label_tytul, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(imie_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(imie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nazwisko_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nazwisko, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(stanowisko_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(stanowisko, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(uprawnienia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(uprawnienia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(panel_danych_logowania, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(data_zatrudnienia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(data_zatrudnienia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(data_zatrudnienia_label1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(wojewodztwo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(data_zatrudnienia_label2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ulica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(data_zatrudnienia_label3, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nrdomu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(wyczysc))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_zatrudnienia_label4, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nrlokalu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_zatrudnienia_label5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(kodpocztowy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_zatrudnienia_label6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(miejscowosc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(dodaj_pracownika, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(1, 1, 1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void close_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_close_buttonActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_close_buttonActionPerformed

    private void clear() {
        imie.setText(null);
        nazwisko.setText(null);
        data_zatrudnienia.setText(null);
        wojewodztwo.setText(null);
        ulica.setText(null);
        nrdomu.setText(null);
        nrlokalu.setText(null);
        kodpocztowy.setText(null);
        miejscowosc.setText(null);
        login.setText(null);
        haslo.setText(null);
        haslo_confirm.setText(null);
        stanowisko.setSelectedItem(null);
        uprawnienia.setSelectedItem(null);
    }
    
    private void wyczyscActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wyczyscActionPerformed
        clear();
    }//GEN-LAST:event_wyczyscActionPerformed

    private void dodaj_pracownikaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dodaj_pracownikaActionPerformed
        PreparedStatement ps;
        ResultSet rs;
        String query_pracownicy = "INSERT INTO PRACOWNICY (ID_PRACOWNIKA, ID_ADRESU, LOGIN, HASLO, IMIE, NAZWISKO, UPRAWNIENIA, KONTO_AKTYWNE, DATA_ZATRUDNIENIA, STANOWISKO) " 
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        String query_adresy = "INSERT INTO ADRESY (ID_ADRESU, WOJEWODZTWO, ULICA, MIEJSCOWOSC, KOD_POCZTOWY, NR_DOMU, NR_LOKALU) VALUES (?, ?, ?, ?, ?, ?, ?)";  
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Object[] options = {"Tak, jestem pewien.","Nie, muszę coś poprawić."};
        int current_max_adress_id = 0;
        int current_max_pracownik_id = Sklep_komputerowy.pracownicy.get(Sklep_komputerowy.pracownicy.size()-1).getIdPracownika();
        
        if (JOptionPane.showOptionDialog(rootPane, "Czy jesteś pewien, że chcesz dodać nowego użytkownika?", "Czy jesteś pewien?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]) ==0)
        {
            try {
                ps = Sklep_komputerowy.conn.prepareStatement("SELECT max(id_adresu) FROM Adresy");
                rs = ps.executeQuery();
                while (rs.next()) current_max_adress_id = rs.getInt("max(id_adresu)");
                current_max_adress_id++;
                
                ps = Sklep_komputerowy.conn.prepareCall(query_adresy);
                ps.setInt(1, current_max_adress_id);
                ps.setString(2, wojewodztwo.getText());
                ps.setString(3, ulica.getText());
                ps.setString(4, miejscowosc.getText());
                ps.setString(5, kodpocztowy.getText());
                ps.setInt(6, Integer.parseInt(nrdomu.getText()));
                ps.setInt(7, Integer.parseInt(nrlokalu.getText()));
                ps.executeQuery();
                
                current_max_pracownik_id++;
                
                ps = Sklep_komputerowy.conn.prepareCall(query_pracownicy);
                ps.setInt(1, current_max_pracownik_id);
                ps.setInt(2, current_max_adress_id);
                ps.setString(3, login.getText());
                ps.setString(4, haslo.getText());
                ps.setString(5, imie.getText());
                ps.setString(6, nazwisko.getText());
                ps.setString(7, uprawnienia.getSelectedItem().toString());
                ps.setString(8, "y");
                ps.setTimestamp(9, timestamp);
                ps.setString(10, stanowisko.getSelectedItem().toString());
                ps.executeQuery();
                
                JOptionPane.showMessageDialog(rootPane, "Dodano nowego użytkownika!");
            } catch (SQLException ex) {
                Logger.getLogger(DodawaniePracownika.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Sklep_komputerowy.wczytajPracownikow();
    }//GEN-LAST:event_dodaj_pracownikaActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DodawaniePracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DodawaniePracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DodawaniePracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DodawaniePracownika.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DodawaniePracownika().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton close_button;
    private javax.swing.JTextField data_zatrudnienia;
    private javax.swing.JLabel data_zatrudnienia_label;
    private javax.swing.JLabel data_zatrudnienia_label1;
    private javax.swing.JLabel data_zatrudnienia_label2;
    private javax.swing.JLabel data_zatrudnienia_label3;
    private javax.swing.JLabel data_zatrudnienia_label4;
    private javax.swing.JLabel data_zatrudnienia_label5;
    private javax.swing.JLabel data_zatrudnienia_label6;
    private javax.swing.JButton dodaj_pracownika;
    private javax.swing.JPasswordField haslo;
    private javax.swing.JPasswordField haslo_confirm;
    private javax.swing.JTextField imie;
    private javax.swing.JLabel imie_label;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField kodpocztowy;
    private javax.swing.JLabel label_tytul;
    private javax.swing.JTextField login;
    private javax.swing.JTextField miejscowosc;
    private javax.swing.JTextField nazwisko;
    private javax.swing.JLabel nazwisko_label;
    private javax.swing.JTextField nrdomu;
    private javax.swing.JTextField nrlokalu;
    private javax.swing.JPanel panel_danych_logowania;
    private javax.swing.JComboBox stanowisko;
    private javax.swing.JLabel stanowisko_label;
    private javax.swing.JTextField ulica;
    private javax.swing.JComboBox uprawnienia;
    private javax.swing.JLabel uprawnienia_label;
    private javax.swing.JTextField wojewodztwo;
    private javax.swing.JButton wyczysc;
    // End of variables declaration//GEN-END:variables
}
