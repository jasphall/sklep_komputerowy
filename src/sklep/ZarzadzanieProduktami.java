package sklep;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class ZarzadzanieProduktami extends javax.swing.JFrame {

    int current_record = 0;
    byte[] imgDane = null;
    
    public ZarzadzanieProduktami() {
        initComponents();
        changeValues();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        zamknij_okno = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        nazwa = new javax.swing.JTextField();
        producent = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        opis = new javax.swing.JTextArea();
        foto = new javax.swing.JPanel();
        obrazek = new javax.swing.JLabel();
        cena = new javax.swing.JTextField();
        dostepne = new javax.swing.JTextField();
        typ = new javax.swing.JTextField();
        seryjny = new javax.swing.JTextField();
        prev = new javax.swing.JButton();
        next = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        dodaj_produkt = new javax.swing.JButton();
        usun_produkt = new javax.swing.JButton();
        odswiez = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        zamknij_okno.setBackground(new java.awt.Color(255, 51, 51));
        zamknij_okno.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        zamknij_okno.setForeground(new java.awt.Color(255, 255, 255));
        zamknij_okno.setText("Zamknij okno");
        zamknij_okno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zamknij_oknoActionPerformed(evt);
            }
        });

        nazwa.setEditable(false);
        nazwa.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        nazwa.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        nazwa.setText("Nazwa produktu: ");

        producent.setEditable(false);
        producent.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        producent.setText("Producent: ");

        opis.setEditable(false);
        opis.setColumns(10);
        opis.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        opis.setRows(5);
        opis.setText("Opis:");
        jScrollPane1.setViewportView(opis);
        opis.setLineWrap(true);

        foto.setBackground(new java.awt.Color(204, 204, 204));
        foto.setMaximumSize(new java.awt.Dimension(197, 180));

        obrazek.setMaximumSize(new java.awt.Dimension(197, 180));
        obrazek.setPreferredSize(new java.awt.Dimension(100, 100));

        javax.swing.GroupLayout fotoLayout = new javax.swing.GroupLayout(foto);
        foto.setLayout(fotoLayout);
        fotoLayout.setHorizontalGroup(
            fotoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(obrazek, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        fotoLayout.setVerticalGroup(
            fotoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(obrazek, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        cena.setEditable(false);
        cena.setFont(new java.awt.Font("Segoe UI", 0, 12)); // NOI18N
        cena.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        cena.setText("Cena brutto: x / Cena netto: y");

        dostepne.setEditable(false);
        dostepne.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        dostepne.setText("Ilość dostępnych sztuk: ");

        typ.setEditable(false);
        typ.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        typ.setText("Typ: ");

        seryjny.setEditable(false);
        seryjny.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        seryjny.setText("Nr seryjny: ");

        prev.setBackground(new java.awt.Color(51, 102, 255));
        prev.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        prev.setForeground(new java.awt.Color(255, 255, 255));
        prev.setText("Poprzedni");
        prev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prevActionPerformed(evt);
            }
        });

        next.setBackground(new java.awt.Color(51, 102, 255));
        next.setFont(new java.awt.Font("Segoe UI", 0, 10)); // NOI18N
        next.setForeground(new java.awt.Color(255, 255, 255));
        next.setText("Następny");
        next.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nazwa, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(producent, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cena)
                    .addComponent(dostepne)
                    .addComponent(typ)
                    .addComponent(seryjny)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(prev, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(next, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(foto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nazwa, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                    .addComponent(cena))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(producent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dostepne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(typ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(seryjny, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(prev, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(next, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
            .addComponent(foto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        dodaj_produkt.setBackground(new java.awt.Color(255, 102, 0));
        dodaj_produkt.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        dodaj_produkt.setForeground(new java.awt.Color(255, 255, 255));
        dodaj_produkt.setText("DODAJ PRODUKT");
        dodaj_produkt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dodaj_produktActionPerformed(evt);
            }
        });

        usun_produkt.setBackground(new java.awt.Color(255, 102, 0));
        usun_produkt.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        usun_produkt.setForeground(new java.awt.Color(255, 255, 255));
        usun_produkt.setText("USUŃ PRODUKT");
        usun_produkt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usun_produktActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(dodaj_produkt, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(usun_produkt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dodaj_produkt, javax.swing.GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE)
                    .addComponent(usun_produkt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        odswiez.setBackground(new java.awt.Color(102, 153, 255));
        odswiez.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        odswiez.setForeground(new java.awt.Color(255, 255, 255));
        odswiez.setText("Odswież");
        odswiez.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                odswiezActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(odswiez)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(zamknij_okno))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(zamknij_okno)
                    .addComponent(odswiez))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void changeValues() {
        nazwa.setText(Sklep_komputerowy.produkty.get(current_record).getNazwa());
        producent.setText("Producent: " + Sklep_komputerowy.produkty.get(current_record).getProducent().getNazwaProducenta());
        cena.setText("Cena netto / brutto: " + Sklep_komputerowy.produkty.get(current_record).getCenaNetto() + "zł / "
                + Sklep_komputerowy.produkty.get(current_record).getCenaBrutto() + " zł");
        dostepne.setText("Ilość dostępnych sztuk: " + Sklep_komputerowy.produkty.get(current_record).getIloscDostepnych());
        typ.setText("Typ: " + Sklep_komputerowy.produkty.get(current_record).getTyp());
        seryjny.setText("Nr seryjny: " + Sklep_komputerowy.produkty.get(current_record).getNrSeryjny());
        opis.setText(Sklep_komputerowy.produkty.get(current_record).getOpis());
        try {
            if (Sklep_komputerowy.produkty.get(current_record).getImg() != null) {
                imgDane = Sklep_komputerowy.produkty.get(current_record).getImg().getBytes(1, 
                    (int)Sklep_komputerowy.produkty.get(current_record).getImg().length());
                BufferedImage image = ImageIO.read(new ByteArrayInputStream(imgDane));
                obrazek.setIcon(new ImageIcon(image));
            }
            else {
                obrazek.setIcon(null);
            }
        } catch (SQLException | IOException ex) {
            Logger.getLogger(ZarzadzanieProduktami.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void zamknij_oknoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zamknij_oknoActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_zamknij_oknoActionPerformed

    private void dodaj_produktActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dodaj_produktActionPerformed
        DodawanieProduktu dp = new DodawanieProduktu();
        dp.setVisible(true);
    }//GEN-LAST:event_dodaj_produktActionPerformed

    private void prevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prevActionPerformed
        if (current_record>0){
            current_record--;
            changeValues();
        }
    }//GEN-LAST:event_prevActionPerformed

    private void nextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextActionPerformed
        if (current_record<Sklep_komputerowy.produkty.size()-1) current_record++;
        changeValues();
    }//GEN-LAST:event_nextActionPerformed

    private void usun_produktActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usun_produktActionPerformed
        Object[] prod = new Object[Sklep_komputerowy.produkty.size()];
        PreparedStatement ps = null;
        ResultSet rs = null;
        int k = 1;
        
        for (int i=0; i<Sklep_komputerowy.produkty.size(); i++){
            prod[i] = "[" + Sklep_komputerowy.produkty.get(i).getNrSeryjny() + "] " 
                    + Sklep_komputerowy.produkty.get(i).getNazwa() + " " 
                    + Sklep_komputerowy.produkty.get(i).getTyp() + " "
                    + Sklep_komputerowy.produkty.get(i).getProducent().getNazwaProducenta();
        }
        
        String p = (String)JOptionPane.showInputDialog(null, "Wybierz produkt, którego chcesz usunąć: ", 
                "Usuwanie produktow", JOptionPane.QUESTION_MESSAGE, null, prod, 0);
        
        while (p.charAt(k) != ']') k++;
        String w = p.substring(1,k);
        
        try {
            String query_produkty = "DELETE FROM Produkty WHERE nr_seryjny = '" + w + "'";
            
            ps = Sklep_komputerowy.conn.prepareCall(query_produkty);
            ps.executeQuery();
            
        } catch (SQLException ex) {
            Logger.getLogger(ZarzadzaniePracownikami.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        JOptionPane.showMessageDialog(rootPane, "Usunieto produkt z bazy!");
        Sklep_komputerowy.wczytajProdukty();
    }//GEN-LAST:event_usun_produktActionPerformed

    private void odswiezActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_odswiezActionPerformed
        Sklep_komputerowy.wczytajProdukty();
        JOptionPane.showMessageDialog(rootPane, "Baza została zaktualizowana!");
    }//GEN-LAST:event_odswiezActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ZarzadzanieProduktami.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ZarzadzanieProduktami.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ZarzadzanieProduktami.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ZarzadzanieProduktami.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new ZarzadzanieProduktami().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField cena;
    private javax.swing.JButton dodaj_produkt;
    private javax.swing.JTextField dostepne;
    private javax.swing.JPanel foto;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField nazwa;
    private javax.swing.JButton next;
    private javax.swing.JLabel obrazek;
    private javax.swing.JButton odswiez;
    private javax.swing.JTextArea opis;
    private javax.swing.JButton prev;
    private javax.swing.JTextField producent;
    private javax.swing.JTextField seryjny;
    private javax.swing.JTextField typ;
    private javax.swing.JButton usun_produkt;
    private javax.swing.JButton zamknij_okno;
    // End of variables declaration//GEN-END:variables
}
