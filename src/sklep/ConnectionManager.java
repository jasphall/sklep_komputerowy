package sklep;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ConnectionManager {
    private static final String adres_bazy = "jdbc:oracle:thin:@localhost:1521:XE";    
    private static final String sterownik = "oracle.jdbc.driver.OracleDriver";   
    private static final String login = "maciek";   
    private static final String haslo = "admin93";
    private static Connection conn;
    
    public static Connection connectToDatabase() {
        try {
            Class.forName(sterownik);
            try {
                conn = DriverManager.getConnection(adres_bazy,login,haslo);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Wyjatek - SQL");
            }
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Wyjatek - blad sterownika.");
        }
        return conn;
    }
}
