package sklep;

public class ProduktZamowienie {
    private int id_produktu;
    private int id_zamowienia;
    private int ilosc;

    public ProduktZamowienie(int id_produktu, int id_zamowienia, int ilosc) {
        this.id_produktu = id_produktu;
        this.id_zamowienia = id_zamowienia;
        this.ilosc = ilosc;
    }

    public int getId_produktu() {
        return id_produktu;
    }

    public void setId_produktu(int id_produktu) {
        this.id_produktu = id_produktu;
    }

    public int getId_zamowienia() {
        return id_zamowienia;
    }

    public void setId_zamowienia(int id_zamowienia) {
        this.id_zamowienia = id_zamowienia;
    }

    public int getIlosc() {
        return ilosc;
    }

    public void setIlosc(int ilosc) {
        this.ilosc = ilosc;
    }
    
    
}
