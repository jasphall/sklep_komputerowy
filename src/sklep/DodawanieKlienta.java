package sklep;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class DodawanieKlienta extends javax.swing.JFrame {
    
    public DodawanieKlienta() {
        initComponents();
        clear();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label_tytul = new javax.swing.JLabel();
        close_button = new javax.swing.JButton();
        imie_label = new javax.swing.JLabel();
        nazwisko_label = new javax.swing.JLabel();
        imie = new javax.swing.JTextField();
        nazwisko = new javax.swing.JTextField();
        nazwafirmy_label = new javax.swing.JLabel();
        nazwafirmy = new javax.swing.JTextField();
        panel_danych_logowania = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        login = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        haslo = new javax.swing.JPasswordField();
        jLabel9 = new javax.swing.JLabel();
        haslo_confirm = new javax.swing.JPasswordField();
        wojewodztwo_label = new javax.swing.JLabel();
        wojewodztwo = new javax.swing.JTextField();
        ulica_label = new javax.swing.JLabel();
        ulica = new javax.swing.JTextField();
        nrdomu_label = new javax.swing.JLabel();
        nrdomu = new javax.swing.JTextField();
        nrlokalu_label = new javax.swing.JLabel();
        nrlokalu = new javax.swing.JTextField();
        kodpocztowy_label = new javax.swing.JLabel();
        kodpocztowy = new javax.swing.JTextField();
        miejscowosc_label = new javax.swing.JLabel();
        miejscowosc = new javax.swing.JTextField();
        dodaj_klienta = new javax.swing.JButton();
        wyczysc = new javax.swing.JButton();
        data_zatrudnienia_label7 = new javax.swing.JLabel();
        regon = new javax.swing.JTextField();
        regon_label = new javax.swing.JLabel();
        nip = new javax.swing.JTextField();
        tel_label = new javax.swing.JLabel();
        tel = new javax.swing.JTextField();
        email_label = new javax.swing.JLabel();
        email = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        label_tytul.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        label_tytul.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_tytul.setText("DODAWANIE NOWEGO KLIENTA");

        close_button.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        close_button.setText("Zamknij okno");
        close_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close_buttonActionPerformed(evt);
            }
        });

        imie_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        imie_label.setText("Imię klienta:");

        nazwisko_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        nazwisko_label.setText("Nazwisko klienta:");

        imie.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        nazwisko.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        nazwafirmy_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        nazwafirmy_label.setText("Nazwa firmy:");

        nazwafirmy.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        panel_danych_logowania.setBackground(new java.awt.Color(255, 255, 255));

        jLabel7.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        jLabel7.setText("Login:");

        login.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        jLabel8.setText("Hasło:");

        haslo.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        jLabel9.setText("Potwierdź hasło:");

        haslo_confirm.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        haslo_confirm.setToolTipText("");

        javax.swing.GroupLayout panel_danych_logowaniaLayout = new javax.swing.GroupLayout(panel_danych_logowania);
        panel_danych_logowania.setLayout(panel_danych_logowaniaLayout);
        panel_danych_logowaniaLayout.setHorizontalGroup(
            panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_danych_logowaniaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(haslo, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
                    .addComponent(haslo_confirm)
                    .addComponent(login))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        panel_danych_logowaniaLayout.setVerticalGroup(
            panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_danych_logowaniaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(login, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(haslo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panel_danych_logowaniaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(haslo_confirm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        wojewodztwo_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        wojewodztwo_label.setText("Województwo:");

        wojewodztwo.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        ulica_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        ulica_label.setText("Ulica:");

        ulica.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        nrdomu_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        nrdomu_label.setText("Nr domu:");

        nrdomu.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        nrlokalu_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        nrlokalu_label.setText("Nr lokalu:");

        nrlokalu.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        kodpocztowy_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        kodpocztowy_label.setText("Kod pocztowy:");

        kodpocztowy.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        miejscowosc_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        miejscowosc_label.setText("Miejscowość:");

        miejscowosc.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        dodaj_klienta.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        dodaj_klienta.setText("Dodaj klienta");
        dodaj_klienta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dodaj_klientaActionPerformed(evt);
            }
        });

        wyczysc.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        wyczysc.setText("Wyczyść");
        wyczysc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wyczyscActionPerformed(evt);
            }
        });

        data_zatrudnienia_label7.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zatrudnienia_label7.setText("REGON:");

        regon.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        regon_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        regon_label.setText("NIP:");

        nip.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        tel_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        tel_label.setText("Nr tel.");

        tel.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        email_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        email_label.setText("E-mail:");

        email.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(label_tytul, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
                        .addGap(83, 83, 83)
                        .addComponent(close_button))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ulica_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ulica, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(nazwisko_label, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(nazwisko, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(imie_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(imie, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(miejscowosc_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(miejscowosc, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(nrlokalu_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(nrlokalu, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(kodpocztowy_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(kodpocztowy, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(nrdomu_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(nrdomu, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(nazwafirmy_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(nazwafirmy, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(data_zatrudnienia_label7, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(regon, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(regon_label, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nip, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGap(132, 132, 132)
                                        .addComponent(email_label, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(37, 37, 37)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(panel_danych_logowania, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(dodaj_klienta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(wyczysc, javax.swing.GroupLayout.Alignment.TRAILING)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(tel_label, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(tel, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(wojewodztwo_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(wojewodztwo, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(close_button)
                    .addComponent(label_tytul, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(imie_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(imie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nazwisko_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nazwisko, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nazwafirmy_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nazwafirmy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_zatrudnienia_label7, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(regon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(regon_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(panel_danych_logowania, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tel_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(email_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(wojewodztwo_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(wojewodztwo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ulica_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ulica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nrdomu_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nrdomu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(wyczysc))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nrlokalu_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nrlokalu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(kodpocztowy_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(kodpocztowy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(miejscowosc_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(miejscowosc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(dodaj_klienta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(1, 1, 1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void close_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_close_buttonActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_close_buttonActionPerformed

    private void clear() {
        imie.setText(null);
        nazwisko.setText(null);
        nazwafirmy.setText(null);
        regon.setText(null);
        nip.setText(null);
        tel.setText(null);
        email.setText(null);
        wojewodztwo.setText(null);
        ulica.setText(null);
        nrdomu.setText(null);
        nrlokalu.setText(null);
        kodpocztowy.setText(null);
        miejscowosc.setText(null);
        login.setText(null);
        haslo.setText(null);
        haslo_confirm.setText(null);
    }
    
    private void wyczyscActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wyczyscActionPerformed
        clear();
    }//GEN-LAST:event_wyczyscActionPerformed

    private void dodaj_klientaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dodaj_klientaActionPerformed
        PreparedStatement ps;
        ResultSet rs;
        String query_pracownicy = "INSERT INTO KLIENCI (ID_KLIENTA, ID_ADRESU, NAZWISKO, IMIE, LOGIN, HASLO, NAZWA_FIRMY, "
                + "REGON, NIP, TELEFON, EMAIL) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        String query_adresy = "INSERT INTO ADRESY (ID_ADRESU, WOJEWODZTWO, ULICA, MIEJSCOWOSC, KOD_POCZTOWY, NR_DOMU, NR_LOKALU) VALUES (?, ?, ?, ?, ?, ?, ?)";  
        Object[] options = {"Tak, jestem pewien.","Nie, muszę coś poprawić."};
        int current_max_adress_id = 0;
        int current_max_klient_id = Sklep_komputerowy.klienci.get(Sklep_komputerowy.klienci.size()-1).getIdKlienta();
        
        if (JOptionPane.showOptionDialog(rootPane, "Czy jesteś pewien, że chcesz dodać nowego użytkownika?", "Czy jesteś pewien?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]) ==0)
        {
            try {
                ps = Sklep_komputerowy.conn.prepareStatement("SELECT max(id_adresu) FROM Adresy");
                rs = ps.executeQuery();
                while (rs.next()) current_max_adress_id = rs.getInt("max(id_adresu)");
                current_max_adress_id++;
                
                ps = Sklep_komputerowy.conn.prepareCall(query_adresy);
                ps.setInt(1, current_max_adress_id);
                ps.setString(2, wojewodztwo.getText());
                ps.setString(3, ulica.getText());
                ps.setString(4, miejscowosc.getText());
                ps.setString(5, kodpocztowy.getText());
                ps.setInt(6, Integer.parseInt(nrdomu.getText()));
                ps.setInt(7, Integer.parseInt(nrlokalu.getText()));
                ps.executeQuery();
                
                current_max_klient_id++;
                
                ps = Sklep_komputerowy.conn.prepareCall(query_pracownicy);
                ps.setInt(1, current_max_klient_id);
                ps.setInt(2, current_max_adress_id);
                ps.setString(3, nazwisko.getText());
                ps.setString(4, imie.getText());
                ps.setString(5, login.getText());
                ps.setString(6, haslo.getText());
                ps.setString(7, nazwafirmy.getText());
                ps.setString(8, regon.getText());
                ps.setString(9, nip.getText());
                ps.setString(10, tel.getText());
                ps.setString(11, email.getText());
                ps.executeQuery();
                
                JOptionPane.showMessageDialog(rootPane, "Dodano nowego użytkownika!");
            } catch (SQLException ex) {
                Logger.getLogger(DodawanieKlienta.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Sklep_komputerowy.wczytajKlientow();
    }//GEN-LAST:event_dodaj_klientaActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DodawanieKlienta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DodawanieKlienta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DodawanieKlienta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DodawanieKlienta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DodawanieKlienta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton close_button;
    private javax.swing.JLabel data_zatrudnienia_label7;
    private javax.swing.JButton dodaj_klienta;
    private javax.swing.JTextField email;
    private javax.swing.JLabel email_label;
    private javax.swing.JPasswordField haslo;
    private javax.swing.JPasswordField haslo_confirm;
    private javax.swing.JTextField imie;
    private javax.swing.JLabel imie_label;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField kodpocztowy;
    private javax.swing.JLabel kodpocztowy_label;
    private javax.swing.JLabel label_tytul;
    private javax.swing.JTextField login;
    private javax.swing.JTextField miejscowosc;
    private javax.swing.JLabel miejscowosc_label;
    private javax.swing.JTextField nazwafirmy;
    private javax.swing.JLabel nazwafirmy_label;
    private javax.swing.JTextField nazwisko;
    private javax.swing.JLabel nazwisko_label;
    private javax.swing.JTextField nip;
    private javax.swing.JTextField nrdomu;
    private javax.swing.JLabel nrdomu_label;
    private javax.swing.JTextField nrlokalu;
    private javax.swing.JLabel nrlokalu_label;
    private javax.swing.JPanel panel_danych_logowania;
    private javax.swing.JTextField regon;
    private javax.swing.JLabel regon_label;
    private javax.swing.JTextField tel;
    private javax.swing.JLabel tel_label;
    private javax.swing.JTextField ulica;
    private javax.swing.JLabel ulica_label;
    private javax.swing.JTextField wojewodztwo;
    private javax.swing.JLabel wojewodztwo_label;
    private javax.swing.JButton wyczysc;
    // End of variables declaration//GEN-END:variables
}
