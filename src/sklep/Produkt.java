package sklep;

import java.sql.Blob;

public final class Produkt {
    private int id_produktu;
    private String nazwa_produktu;
    private String typ;
    private String nr_seryjny;
    private String opis;
    private Blob img;
    private float cena_brutto;
    private float cena_netto;
    private int dostepne;
    private Producent p;
    
    public int getIdProduktu(){
        return id_produktu;
    }
    
    public void setIdProduktu(int id_produktu){
        this.id_produktu = id_produktu;
    }
    
    public String getNazwa(){
        return nazwa_produktu;
    }
    
    public void setNazwa(String nazwa){
        this.nazwa_produktu = nazwa;
    }
    
    public String getTyp(){
        return typ;
    }
    
    public void setTyp(String typ){
        this.typ = typ;
    }
    
    public String getNrSeryjny(){
        return nr_seryjny;
    }
    
    public void setNrSeryjny(String nr){
        this.nr_seryjny = nr;
    }
    
    public String getOpis(){
        return opis;
    }
    
    public void setOpis(String opis){
        this.opis = opis;
    }
    
    public Blob getImg(){
        return img;
    }
    
    public void setImg(Blob img){
        this.img = img;
    }
    
    public float getCenaBrutto(){
        return cena_brutto;
    }
    
    public void setCenaBrutto(float cena){
        this.cena_brutto = cena;
    }
    
    public float getCenaNetto(){
        return cena_netto;
    }
    
    public void setCenaNetto(float cena){
        this.cena_netto = cena;
    }
    
    public int getIloscDostepnych(){
        return dostepne;
    }
    
    public void setIloscDostepnych(int dostepne){
        this.dostepne = dostepne;
    }
    
    public Producent getProducent(){
        return p;
    }

    public Produkt(int id_produktu, String nazwa_produktu, String typ, String nr_seryjny, String opis, Blob img, float cena_brutto, float cena_netto, int dostepne, Producent p) {
        this.id_produktu = id_produktu;
        this.nazwa_produktu = nazwa_produktu;
        this.typ = typ;
        this.nr_seryjny = nr_seryjny;
        this.opis = opis;
        this.img = img;
        this.cena_brutto = cena_brutto;
        this.cena_netto = cena_netto;
        this.dostepne = dostepne;
        this.p = p;
    }
    
    
}
