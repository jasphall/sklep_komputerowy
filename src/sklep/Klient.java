package sklep;

public final class Klient extends Osoba {
    private int id_klienta;
    private String nazwa_firmy;
    private String regon;
    private String nip;
    private String telefon;
    private String email;
    
    public int getIdKlienta(){
        return id_klienta;
    }
    
    public void setIdKlienta(int id_klienta){
        this.id_klienta = id_klienta;
    }
    
    public String getNazwaFirmy(){
        return nazwa_firmy;
    }
    
    public void setNazwaFirmy(String nazwa_firmy){
        this.nazwa_firmy = nazwa_firmy;
    }
    
    public String getRegon(){
        return regon;
    }
    
    public void setRegon(String regon){
        this.regon = regon;
    }
    
    public String getNip(){
        return nip;
    }
    
    public void setNip(String nip){
        this.nip = nip;
    }
    
    public String getTelefon(){
        return telefon;
    }
    
    public void setTelefon(String telefon){
        this.telefon = telefon;
    }
    
    public String getEmail(){
        return email;
    }
    
    public void setEmail(String email){
        this.email = email;
    }

    public Klient(int id_klienta, String nazwa_firmy, String regon, String nip, String telefon, String email, String imie, String nazwisko, String login, String haslo, String wojewodztwo, String ulica, String miejscowosc, String kod_pocztowy, int nr_domu, int nr_lokalu) {
        super(imie, nazwisko, login, haslo, wojewodztwo, ulica, miejscowosc, kod_pocztowy, nr_domu, nr_lokalu);
        this.id_klienta = id_klienta;
        this.nazwa_firmy = nazwa_firmy;
        this.regon = regon;
        this.nip = nip;
        this.telefon = telefon;
        this.email = email;
    }
}
