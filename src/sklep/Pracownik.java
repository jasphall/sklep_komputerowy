package sklep;

public final class Pracownik extends Osoba {
    private int id_pracownika;
    private String uprawnienia;
    private String konto_aktywne; //  or boolean
    private String data_zatrudnienia;
    private String stanowisko;
    
    public int getIdPracownika(){
        return id_pracownika;
    }
    
    public void setIdPracownika(int id_pracownika){
        this.id_pracownika = id_pracownika;
    }
    
    public String getUprawnienia(){
        return uprawnienia;
    }
    
    public void setUprawnienia(String uprawnienia){
        this.uprawnienia = uprawnienia;
    }
    
    public String getKontoAktywne(){
        return konto_aktywne;
    }
    
    public void setKontoAktywne(String konto_aktywne){
        this.konto_aktywne = konto_aktywne;
    }
    
    public String getDataZatrudnienia(){
        return data_zatrudnienia;
    }
    
    public void setDataZatrudnienia(String data_zatrudnienia){
        this.data_zatrudnienia = data_zatrudnienia;
    }
    
    public String getStanowisko(){
        return stanowisko;
    }
    
    public void setStanowisko(String stanowisko){
        this.stanowisko = stanowisko;
    }

    public Pracownik(int id_pracownika, String uprawnienia, String konto_aktywne, String data_zatrudnienia, String stanowisko, String imie, String nazwisko, String login, String haslo, String wojewodztwo, String ulica, String miejscowosc, String kod_pocztowy, int nr_domu, int nr_lokalu) {
        super(imie, nazwisko, login, haslo, wojewodztwo, ulica, miejscowosc, kod_pocztowy, nr_domu, nr_lokalu);
        this.id_pracownika = id_pracownika;
        this.uprawnienia = uprawnienia;
        this.konto_aktywne = konto_aktywne;
        this.data_zatrudnienia = data_zatrudnienia;
        this.stanowisko = stanowisko;
    }
    
    
}
