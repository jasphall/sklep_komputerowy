package sklep;


public class PanelLogowania extends javax.swing.JFrame {

    public PanelLogowania() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        login_field = new javax.swing.JTextField();
        pass_field = new javax.swing.JPasswordField();
        login_label = new javax.swing.JLabel();
        pass_label = new javax.swing.JLabel();
        zaloguj_przycisk = new javax.swing.JButton();
        pole_sprawdzajace = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        login_field.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        login_field.setToolTipText("");

        pass_field.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        login_label.setText("Nazwa użytkownika:");

        pass_label.setText("Hasło użytkownika:");

        zaloguj_przycisk.setText("Zaloguj do systemu");
        zaloguj_przycisk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zaloguj_przyciskActionPerformed(evt);
            }
        });

        pole_sprawdzajace.setEditable(false);
        pole_sprawdzajace.setBackground(new java.awt.Color(255, 255, 255));
        pole_sprawdzajace.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pole_sprawdzajace)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(pass_label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(login_label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(login_field)
                            .addComponent(pass_field)
                            .addComponent(zaloguj_przycisk, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(176, 176, 176))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(87, 87, 87)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(login_label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(login_field, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pass_field, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(pass_label, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(zaloguj_przycisk, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addComponent(pole_sprawdzajace, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void zaloguj_przyciskActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zaloguj_przyciskActionPerformed
        String login;
        String haslo;
        login = login_field.getText();
        haslo = pass_field.getText();
        PanelKlienta pk;
        PanelPracownika pp;
        
        if (Sklep_komputerowy.autoryzacjaUzytkownika(login, haslo) == 0){
            this.setVisible(false);
            pp = new PanelPracownika();
            pp.setVisible(true);
        }
        else if (Sklep_komputerowy.autoryzacjaUzytkownika(login, haslo) == 1){
            this.setVisible(false);
            pk = new PanelKlienta();
            pk.setVisible(true);
        }
        else pole_sprawdzajace.setText("BLAD LOGOWANIA!");
    }//GEN-LAST:event_zaloguj_przyciskActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PanelLogowania().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField login_field;
    private javax.swing.JLabel login_label;
    private javax.swing.JPasswordField pass_field;
    private javax.swing.JLabel pass_label;
    private javax.swing.JTextField pole_sprawdzajace;
    private javax.swing.JButton zaloguj_przycisk;
    // End of variables declaration//GEN-END:variables
}
