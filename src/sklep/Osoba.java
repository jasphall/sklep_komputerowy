package sklep;

public abstract class Osoba {
    protected String imie;
    protected String nazwisko;
    protected String login;
    protected String haslo;
    protected String wojewodztwo;
    protected String ulica;
    protected String miejscowosc;
    protected String kod_pocztowy;
    protected int nr_domu;
    protected int nr_lokalu;
    
    public String getImie(){
        return imie;
    }
    
    public void setImie(String imie){
        this.imie = imie;
    }
    
    public String getNazwisko(){
        return nazwisko;
    }
    
    public void setNazwisko(String nazwisko){
        this.nazwisko = nazwisko;
    }
    
    public String getLogin(){
        return login;
    }
    
    public void setLogin(String login){
        this.login = login;
    }
    
    public String getHaslo(){
        return haslo;
    }
    
    public void setHaslo(String haslo){
        this.haslo = haslo;
    }
    
    public String getWojewodztwo(){
        return wojewodztwo;
    }
    
    public void setWojewodztwo(String wojewodztwo){
        this.wojewodztwo = wojewodztwo;
    }
    
    public String getUlica(){
        return ulica;
    }
    
    public void setUlica(String ulica){
        this.ulica = ulica;
    }
    
    public String getMiejcowosc(){
        return miejscowosc;
    }
    
    public void setMiejscowosc(String miejscowosc){
        this.miejscowosc = miejscowosc;
    }
    
    public String getKodPocztowy(){
        return kod_pocztowy;
    }
    
    public void setKodPocztowy(String kod_pocztowy){
        this.kod_pocztowy = kod_pocztowy;
    }
    
    public int getNrDomu(){
        return nr_domu;
    }
    
    public void setNrDomu(int nr_domu){
        this.nr_domu = nr_domu;
    }
    
    public int getNrLokalu(){
        return nr_lokalu;
    }
    
    public void setNrLokalu(int nr_lokalu){
        this.nr_lokalu = nr_lokalu;
    }

    public Osoba(String imie, String nazwisko, String login, String haslo, String wojewodztwo, String ulica, String miejscowosc, String kod_pocztowy, int nr_domu, int nr_lokalu) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.login = login;
        this.haslo = haslo;
        this.wojewodztwo = wojewodztwo;
        this.ulica = ulica;
        this.miejscowosc = miejscowosc;
        this.kod_pocztowy = kod_pocztowy;
        this.nr_domu = nr_domu;
        this.nr_lokalu = nr_lokalu;
    }
    
    
    
}
