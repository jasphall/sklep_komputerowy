package sklep;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class SzczegolyZamowienia extends javax.swing.JFrame {
    
    public static List<Integer> ids = new ArrayList<>();
    private int current_product = 0;
    private int current_record = ZarzadzanieZamowieniami.current_record;
    
    public SzczegolyZamowienia() {
        initComponents();
        ids.clear();
        ileProduktow();
        zmienWartosci();
        changeValues();
    }

    private void changeValues() {
        firma.setText(Sklep_komputerowy.zamowienia.get(current_record).getK().getNazwaFirmy());
        przedstawiciel.setText(Sklep_komputerowy.zamowienia.get(current_record).getK().getImie() + " " 
                + Sklep_komputerowy.zamowienia.get(current_record).getK().getNazwisko());
        telefon.setText(Sklep_komputerowy.zamowienia.get(current_record).getK().getTelefon());
        email.setText(Sklep_komputerowy.zamowienia.get(current_record).getK().getEmail());
        data_zlozenia.setText(Sklep_komputerowy.zamowienia.get(current_record).getData_zamowienia());
        data_przyjecia.setText(Sklep_komputerowy.zamowienia.get(current_record).getData_przyjecia_zamowienia());
        platnosc.setText(Sklep_komputerowy.zamowienia.get(current_record).getPlatnosc());
        data_wysylki.setText(Sklep_komputerowy.zamowienia.get(current_record).getData_wysylki());
        stan.setText(Boolean.toString(Sklep_komputerowy.zamowienia.get(current_record).getCzy_zrealizowano()));
        data_realizacji.setText(Sklep_komputerowy.zamowienia.get(current_record).getData_realizacji());
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        close_button = new javax.swing.JButton();
        nazwa_produktu = new javax.swing.JTextField();
        ilosc_sztuk = new javax.swing.JTextField();
        cena = new javax.swing.JTextField();
        cena_lacznie = new javax.swing.JTextField();
        produkt = new javax.swing.JLabel();
        ilosc_Sztuk = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        prev_record = new javax.swing.JButton();
        next_record = new javax.swing.JButton();
        telefon = new javax.swing.JTextField();
        email_label = new javax.swing.JLabel();
        email = new javax.swing.JTextField();
        data_zlozenia_label = new javax.swing.JLabel();
        data_zlozenia = new javax.swing.JTextField();
        data_przyjecia_label = new javax.swing.JLabel();
        data_przyjecia = new javax.swing.JTextField();
        platnosc_label = new javax.swing.JLabel();
        platnosc = new javax.swing.JTextField();
        data_wysylki_label = new javax.swing.JLabel();
        data_wysylki = new javax.swing.JTextField();
        stan_label = new javax.swing.JLabel();
        stan = new javax.swing.JTextField();
        data_realizacji_label = new javax.swing.JLabel();
        data_realizacji = new javax.swing.JTextField();
        nazwa_firmy_label = new javax.swing.JLabel();
        przedstawiciel_label = new javax.swing.JLabel();
        firma = new javax.swing.JTextField();
        przedstawiciel = new javax.swing.JTextField();
        telefon_label = new javax.swing.JLabel();
        zamowione_produkty = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        close_button.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        close_button.setText("Zamknij okno");
        close_button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                close_buttonActionPerformed(evt);
            }
        });

        nazwa_produktu.setEditable(false);
        nazwa_produktu.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        nazwa_produktu.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        ilosc_sztuk.setEditable(false);
        ilosc_sztuk.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        ilosc_sztuk.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        cena.setEditable(false);
        cena.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        cena.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        cena_lacznie.setEditable(false);
        cena_lacznie.setFont(new java.awt.Font("Segoe UI Light", 1, 14)); // NOI18N
        cena_lacznie.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        produkt.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        produkt.setText("PRODUKT");

        ilosc_Sztuk.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        ilosc_Sztuk.setText("ILOŚĆ SZTUK");

        jLabel1.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        jLabel1.setText("CENA [1 szt.]");

        jLabel2.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        jLabel2.setText("CENA [łącznie]");

        prev_record.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        prev_record.setText("Poprzedni");
        prev_record.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prev_recordActionPerformed(evt);
            }
        });

        next_record.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        next_record.setText("Następny");
        next_record.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                next_recordActionPerformed(evt);
            }
        });

        telefon.setEditable(false);
        telefon.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        email_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        email_label.setText("E-mail przedstawiciela:");

        email.setEditable(false);
        email.setFont(new java.awt.Font("Segoe UI Light", 2, 11)); // NOI18N

        data_zlozenia_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_zlozenia_label.setText("Data złożenia zamówienia:");

        data_zlozenia.setEditable(false);
        data_zlozenia.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_przyjecia_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_przyjecia_label.setText("Data przyjęcia zamówienia:");

        data_przyjecia.setEditable(false);
        data_przyjecia.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        platnosc_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        platnosc_label.setText("Rodzaj płatności:");

        platnosc.setEditable(false);
        platnosc.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_wysylki_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_wysylki_label.setText("Data wysyłki:");

        data_wysylki.setEditable(false);
        data_wysylki.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        stan_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        stan_label.setText("Stan realizacji zamówienia:");

        stan.setEditable(false);
        stan.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        data_realizacji_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        data_realizacji_label.setText("Data realizacji zamówienia:");

        data_realizacji.setEditable(false);
        data_realizacji.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        nazwa_firmy_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        nazwa_firmy_label.setText("Nazwa firmy:");

        przedstawiciel_label.setFont(new java.awt.Font("Segoe UI Light", 0, 10)); // NOI18N
        przedstawiciel_label.setText("Imię i nazwisko przedstawiciela:");

        firma.setEditable(false);
        firma.setFont(new java.awt.Font("Segoe UI Light", 1, 11)); // NOI18N

        przedstawiciel.setEditable(false);
        przedstawiciel.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N

        telefon_label.setFont(new java.awt.Font("Segoe UI Light", 0, 11)); // NOI18N
        telefon_label.setText("Nr telefonu przedstawiciela:");

        zamowione_produkty.setFont(new java.awt.Font("Segoe UI Light", 1, 18)); // NOI18N
        zamowione_produkty.setText("ZAMÓWIONE PRODUKTY");

        jLabel3.setFont(new java.awt.Font("Segoe UI Light", 1, 14)); // NOI18N
        jLabel3.setText("SZCZEGÓŁY ZAMÓWIENIA");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(close_button))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(nazwa_firmy_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(44, 44, 44)
                                .addComponent(firma, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(przedstawiciel_label, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(email_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(telefon_label, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(data_zlozenia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(22, 22, 22)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(telefon, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(data_zlozenia, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(przedstawiciel, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(stan_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(data_realizacji_label, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(22, 22, 22)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(stan, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(data_realizacji, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(platnosc_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(data_wysylki_label, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(data_przyjecia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(data_przyjecia, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(platnosc, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(data_wysylki, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(12, 12, 12))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(zamowione_produkty, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(prev_record, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(next_record, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nazwa_produktu, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(produkt))
                                .addGap(34, 34, 34)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(cena, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(38, 38, 38)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(ilosc_sztuk, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ilosc_Sztuk))
                                .addGap(34, 34, 34)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(cena_lacznie, javax.swing.GroupLayout.DEFAULT_SIZE, 84, Short.MAX_VALUE))))
                        .addGap(54, 54, 54)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(close_button)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(nazwa_firmy_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(firma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(przedstawiciel_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(przedstawiciel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(telefon_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(telefon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_przyjecia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(data_przyjecia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(platnosc_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(platnosc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_wysylki_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(data_wysylki, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(email_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_zlozenia_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(data_zlozenia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(stan_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(stan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(data_realizacji_label, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(data_realizacji, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 3, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(zamowione_produkty, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(produkt)
                            .addComponent(jLabel1)
                            .addComponent(ilosc_Sztuk)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cena_lacznie)
                            .addComponent(cena, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(nazwa_produktu))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(next_record)
                            .addComponent(prev_record))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(ilosc_sztuk, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void close_buttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_close_buttonActionPerformed
        this.setVisible(false);
    }//GEN-LAST:event_close_buttonActionPerformed

    private void next_recordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_next_recordActionPerformed
        if (current_product<ids.size()-1) current_product++;
        zmienWartosci();
    }//GEN-LAST:event_next_recordActionPerformed

    private void prev_recordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prev_recordActionPerformed
        if (current_product>0){
            current_product--;
            zmienWartosci();
        }
    }//GEN-LAST:event_prev_recordActionPerformed

    private int ileProduktow() {
        int i=0;
        for (ProduktZamowienie pz : Sklep_komputerowy.prod_zam) {
            if (pz.getId_zamowienia() == ZarzadzanieZamowieniami.current_record+1) {
                ids.add(pz.getId_produktu());
                i++;
            }
        }
        return i;
    }
    
    private void zmienWartosci() {
        float temp_cena = 0;
        
        for (Produkt p : Sklep_komputerowy.produkty) {
            if (p.getIdProduktu() == ids.get(current_product)) {
                nazwa_produktu.setText(p.getNazwa() + " " + p.getTyp());
                cena.setText(String.valueOf(p.getCenaBrutto()) + " zł");
                temp_cena = p.getCenaBrutto();
            }
        }
        for (ProduktZamowienie pz : Sklep_komputerowy.prod_zam) {
            if (pz.getId_zamowienia() == ZarzadzanieZamowieniami.current_record+1) {
                if (ids.get(current_product) == pz.getId_produktu()) {
                    ilosc_sztuk.setText(String.valueOf(pz.getIlosc()));
                    cena_lacznie.setText(String.valueOf(temp_cena*pz.getIlosc()) + " zł");
                }
            }
        }
    }
    
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SzczegolyZamowienia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SzczegolyZamowienia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SzczegolyZamowienia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SzczegolyZamowienia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(() -> {
            new SzczegolyZamowienia().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField cena;
    private javax.swing.JTextField cena_lacznie;
    private javax.swing.JButton close_button;
    private javax.swing.JTextField data_przyjecia;
    private javax.swing.JLabel data_przyjecia_label;
    private javax.swing.JTextField data_realizacji;
    private javax.swing.JLabel data_realizacji_label;
    private javax.swing.JTextField data_wysylki;
    private javax.swing.JLabel data_wysylki_label;
    private javax.swing.JTextField data_zlozenia;
    private javax.swing.JLabel data_zlozenia_label;
    private javax.swing.JTextField email;
    private javax.swing.JLabel email_label;
    private javax.swing.JTextField firma;
    private javax.swing.JLabel ilosc_Sztuk;
    private javax.swing.JTextField ilosc_sztuk;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel nazwa_firmy_label;
    private javax.swing.JTextField nazwa_produktu;
    private javax.swing.JButton next_record;
    private javax.swing.JTextField platnosc;
    private javax.swing.JLabel platnosc_label;
    private javax.swing.JButton prev_record;
    private javax.swing.JLabel produkt;
    private javax.swing.JTextField przedstawiciel;
    private javax.swing.JLabel przedstawiciel_label;
    private javax.swing.JTextField stan;
    private javax.swing.JLabel stan_label;
    private javax.swing.JTextField telefon;
    private javax.swing.JLabel telefon_label;
    private javax.swing.JLabel zamowione_produkty;
    // End of variables declaration//GEN-END:variables
}
